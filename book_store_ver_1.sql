/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `special` tinyint(1) DEFAULT NULL,
  `sale_off` int(3) DEFAULT NULL,
  `picture` text DEFAULT NULL,
  `created` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `cart` (
  `id` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `books` text DEFAULT NULL,
  `prices` text DEFAULT NULL,
  `quantities` text DEFAULT NULL,
  `names` text DEFAULT NULL,
  `pictures` text DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `canceled` tinyint(1) DEFAULT 0,
  `confirmed` tinyint(1) DEFAULT 0,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `order_person` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_name` (`user_id`),
  KEY `books` (`books`(768)),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `picture` text DEFAULT NULL,
  `created` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `ordering` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `group_acp` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `privilege_id` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pre_order` (
  `id_pre` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `books` text DEFAULT NULL,
  `prices` text DEFAULT NULL,
  `quantities` text DEFAULT NULL,
  PRIMARY KEY (`id_pre`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `pre_order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `register_ip` varchar(255) DEFAULT NULL,
  `visited_date` datetime DEFAULT NULL,
  `visited_ip` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

INSERT INTO `book` (`id`, `name`, `description`, `price`, `special`, `sale_off`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `category_id`) VALUES
(7, 'MnEABiW3', 'mJQl4\nLAVr01YEDWIsOCRd2xcM7fkgqZUn6NtHyh95piGvjewTb Szuo8aPF', 1103279, 1, 0, NULL, '2022-11-24', NULL, '2022-12-04', 1, 1, 1, 11);
INSERT INTO `book` (`id`, `name`, `description`, `price`, `special`, `sale_off`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `category_id`) VALUES
(8, 'V4JujW', 'hD0x3qcFAz\n25XI gNQrbvtWnHK9a74CuflM6ZPOeiEdoGVkyw1LYjJBUTsR', 639977, 0, 0, NULL, '2022-11-24', NULL, '2022-12-04', 1, 1, 6, 12);
INSERT INTO `book` (`id`, `name`, `description`, `price`, `special`, `sale_off`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `category_id`) VALUES
(9, 'Rhtio4a', '5Miwn9XoVNGtEATkdb\njhKemzruc10x C4sLvfWZyRPB3YO6ql8DJIgQUaH7', 457402, 1, 0, NULL, '2022-11-24', NULL, '2022-12-04', 1, 1, 3, 5);
INSERT INTO `book` (`id`, `name`, `description`, `price`, `special`, `sale_off`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `category_id`) VALUES
(10, 'v7f6SIB', 'SUX9hdwTAyxnWim5KbrelBGYMEDZJ27toF1zHI3O8RcjagpfkqN\r\nVC4L6vPs', 1066640, 0, 20, '', '2022-12-04', NULL, '2022-12-04', 1, 1, 4, 11),
(12, 'YxlGH5N4', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \"Content here, content here\", making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \"lorem ipsum\" will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 837531, 1, 13, '', '2022-11-28', NULL, '2022-12-04', 1, 1, 66, 3),
(13, 'GTliKno', 'ahpUGVyE2A1vir30eC56DsIwfSQ4unkqdNZm9P7jBF XcWYozbTRl8\nxHMJL', 1942625, 0, 13, NULL, '2022-11-24', NULL, '2022-11-24', 1, 1, 36, 3),
(14, 'tg0TorEn9L', 'GHBo6YfgT7sDlWc0Okx1dJFtqw\n4 32VAQL9bUjmnzSe5piZuhIXaP8NvryM', 1519287, 0, 1, NULL, '2022-11-24', NULL, '2022-11-24', 1, 0, 29, 3),
(15, 'gzvi7', 'l5Ze QPrkXIDcW3mfBHd\nFi1sbExKyUOA8TpVqu4ato0MhN9nvC62R7zJLgG', 860630, 0, 0, NULL, '2022-11-24', NULL, NULL, NULL, 1, 67, 7),
(16, 'aLe46Sj', 'RXFNTLsyze\nZKmY2M78IUhu15OCgbPvxajp49to S3lciBQwq6kE0JDAGHnd', 172414, 0, 10, NULL, '2022-11-24', NULL, '2022-11-24', 1, 1, 60, 7),
(17, '8dRPniaHBQ', '8r0Gz3MSkmRlxBoXt2DgZp9nNOP7wEVisevf1 FJTHWQI\ndyYhCbLuq5UAaj', 611335, 1, 9, NULL, '2022-11-24', NULL, NULL, NULL, 1, 69, 6),
(18, 'ZBkWyrJm', 'LfEV8OsXxPJzlb4tiUQj37IpKqNu5dC6GF1yk DhR2ew0aYrvgZWnBSHTom9', 1024790, 0, 22, NULL, '2022-11-24', NULL, NULL, NULL, 0, 53, 20),
(19, 'SDi ', 'is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industrys standarddummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book', 1039755, 0, 13, '', '2022-11-29', NULL, '2022-11-29', 1, 1, 36, 5),
(20, 'Mxi8lAFdu', 'tgIKAfDd2r9WHwsOcmve75iLhUJSXxoZFnYuj4kzaMCB8V0\n6lQb GTNEP3q', 1039893, 0, 32, NULL, '2022-11-24', NULL, NULL, NULL, 0, 57, 3),
(21, 'sBqly4f5gA', 'oNft\nPEZeJRS8n5UAIzGdb7DyQpFYc9B2 CqhHrVMKaLxOg36wluvTXkji04', 1165694, 1, 23, NULL, '2022-11-24', NULL, NULL, NULL, 1, 22, 3),
(22, 'flH8D', 'W36d9iIGmJF gtB8XUYceVTMu0EkQr5SHxphAav1q\nbyRLlfZo7Cw2KDOjsP', 1503501, 0, 33, NULL, '2022-11-24', NULL, NULL, NULL, 0, 12, 18),
(23, 'osfMCQju', 'a2Cuih0XOd16tfn bcQEgBeT8ISJvm57AlKy3DM4pFzqoLwkrZsV9jRUNYxP', 1443970, 1, 13, NULL, '2022-11-24', NULL, NULL, NULL, 1, 46, 5),
(24, 'lBLrwFWy', 'lh5ADcYyWs18GeJ97vbNXpFrxjH3USPL\ntmTiEfguzIZnVBo4adCK0q MOk6', 246359, 0, 13, NULL, '2022-11-24', NULL, NULL, NULL, 0, 43, 5),
(25, 'rlacghbSI7', 'Hn4ZCX6kutIBmjv7wdL8NY0MAcqVeDKbQ \n3JSrOg5ohzTlsiapU1xy2GfRW', 1893560, 0, 25, NULL, '2022-11-24', NULL, NULL, NULL, 1, 65, 3),
(26, 'lwLtyCrH', '5nFi1udPXRTCpNlbJevHj8zf3gcmYSLaKE9MO0 o2h7QBwrk6UxG\nqZ4tAWD', 994388, 0, 7, NULL, '2022-11-24', NULL, NULL, NULL, 0, 25, 6),
(27, 'YkIsvK7', '6wmjboyV0uCJ3v12L5ROdh\nZaGTQeSDMErP9XIAxNfU8spKt7iYzFkqBng4 ', 1124180, 0, 17, NULL, '2022-11-24', NULL, NULL, NULL, 1, 46, 6),
(28, 'gCA ', 's02kThP7Hvu14j3iaNzCFRBp9DxtrKo OGcXZIUeAgbdfM\n5lmEy8wSLWYJV', 1512862, 1, 22, NULL, '2022-11-24', NULL, NULL, NULL, 0, 77, 19),
(29, 'BNT04gwE', 'hi0qCO\nUVoLd74KF31QPmDRp5rBtNkXabWux AcEHYMwv6fI8zsj2nG9gJly', 832769, 0, 0, NULL, '2022-11-24', NULL, NULL, NULL, 1, 31, 20),
(30, 'DgK72', 'BM0nAEoQT4VxfekF8rcglWmp62u 3wKXtHILGOhRsP5CU1bNazvdq7J9Sji\n', 1881569, 1, 20, NULL, '2022-11-24', NULL, NULL, NULL, 0, 88, 20),
(31, 'yfCrUi', 'O0Eu3zCDWgHMkUeN8olGFaqJKZBd9541bVSyhwPifrLQ\nAxtmTYv26 jI7RX', 1438416, 1, 1, NULL, '2022-11-24', NULL, NULL, NULL, 1, 56, 4),
(32, 'dKV7XymHW', 'pfa\nJ9VQiFN I2ZCXjwKvq7r4GboLhAl38RxyEgDuYm0kHzTd5PctOM1UBWS', 860329, 0, 22, NULL, '2022-11-24', NULL, '2022-11-25', 1, 1, 45, 7),
(33, 'FRkS', 'FyuTHkfsBPjxI3 mEtq97hb6evgp1YS2orXJRNnad8DZL5i4CzV0lGWAUw\nc', 1516827, 0, 26, NULL, '2022-11-24', NULL, NULL, NULL, 1, 33, 20),
(42, 'Music', 'is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industrys standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book', 25000, 1, 0, '', '2022-11-28', 1, '2022-11-28', 1, 1, 12, 4),
(45, 'conan', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad m', 50000, 0, 0, '60x90-OsCYr.png', '2022-12-04', 1, '2022-12-04', 1, 1, 4, 18);

INSERT INTO `cart` (`id`, `user_id`, `books`, `prices`, `quantities`, `names`, `pictures`, `status`, `date`, `canceled`, `confirmed`, `address`, `phone`, `order_person`) VALUES
('2tFCEmg', 1, '[\"10\"]', '[\"853312\"]', '[\"3\"]', '[\"v7f6SIB\"]', '[\"\"]', 1, '2022-12-02 15:07:54', 0, 1, NULL, NULL, NULL);
INSERT INTO `cart` (`id`, `user_id`, `books`, `prices`, `quantities`, `names`, `pictures`, `status`, `date`, `canceled`, `confirmed`, `address`, `phone`, `order_person`) VALUES
('4RcMHGE', 8, '[\"13\"]', '[\"1690083.75\"]', '[\"1\"]', '[\"GTliKno\"]', '[\"\"]', 1, '2022-12-03 17:24:07', 0, 1, NULL, NULL, NULL);
INSERT INTO `cart` (`id`, `user_id`, `books`, `prices`, `quantities`, `names`, `pictures`, `status`, `date`, `canceled`, `confirmed`, `address`, `phone`, `order_person`) VALUES
('AgZXsdJ', 1, '[\"9\",\"42\"]', '[\"457402\",\"25000\"]', '[\"1\",\"1\"]', '[\"Rhtio4a\",\"Music\"]', '[\"\",\"\"]', 0, '2022-12-02 15:09:32', 0, 1, NULL, NULL, NULL);
INSERT INTO `cart` (`id`, `user_id`, `books`, `prices`, `quantities`, `names`, `pictures`, `status`, `date`, `canceled`, `confirmed`, `address`, `phone`, `order_person`) VALUES
('BoiVS8z', 8, '[\"10\"]', '[\"853312\"]', '[\"1\"]', '[\"v7f6SIB\"]', '[\"\"]', 1, '2022-12-03 17:33:17', 0, 1, NULL, NULL, NULL),
('FdiDtLM', 8, '[\"10\"]', '[\"853312\"]', '[\"8\"]', '[\"v7f6SIB\"]', '[\"\"]', 1, '2022-12-03 16:04:22', 0, 1, NULL, NULL, NULL),
('hOHjfGo', 9, '[\"12\"]', '[\"728651.97\"]', '[\"1\"]', '[\"YxlGH5N4\"]', '[\"\"]', 0, '2022-12-04 10:52:29', 0, 1, '1851 Aufderhar Crossing', '(+84)7455098960', 'Tran Phuc Thinh'),
('hSErkfK', 1, '[\"12\",\"40\"]', '[\"728651.97\",\"225000\"]', '[\"1\",\"1\"]', '[\"YxlGH5N4\",\"Dien tu thong tin\"]', '[\"\",\"60x90-lGKzH.jpg\"]', 0, '2022-11-30 17:51:10', 0, 1, NULL, NULL, NULL),
('Jbhq0IW', 1, '[\"12\",\"40\"]', '[\"728651.97\",\"225000\"]', '[\"1\",\"1\"]', '[\"YxlGH5N4\",\"Dien tu thong tin\"]', '[\"\",\"60x90-lGKzH.jpg\"]', 1, '2022-11-30 17:52:43', 0, 1, NULL, NULL, NULL),
('kYI8pBe', 1, '[\"10\",\"12\",\"13\"]', '[\"853312\",\"728651.97\",\"1690083.75\"]', '[\"4\",\"1\",\"1\"]', '[\"v7f6SIB\",\"YxlGH5N4\",\"GTliKno\"]', '[\"\",\"\",\"\"]', 1, '2022-12-02 15:07:09', 0, 1, NULL, NULL, NULL),
('nWUSeDJ', 8, '[\"10\"]', '[\"853312\"]', '[\"1\"]', '[\"v7f6SIB\"]', '[\"\"]', 1, '2022-12-03 17:28:15', 0, 1, NULL, NULL, NULL),
('OLP6ZMk', 12, '[\"10\"]', '[\"853312\"]', '[\"1\"]', '[\"v7f6SIB\"]', '[\"\"]', 0, '2022-12-04 16:19:40', 1, 1, '1851 Aufderhar Crossing', '(+84)7455098960', 'member6'),
('pCMe3jW', 8, '[\"10\",\"12\"]', '[\"853312\",\"728651.97\"]', '[\"1\",\"1\"]', '[\"v7f6SIB\",\"YxlGH5N4\"]', '[\"\",\"\"]', 0, '2022-12-03 15:52:59', 1, 1, NULL, NULL, NULL),
('PgeB9Q5', 11, '[\"10\"]', '[\"853312\"]', '[\"1\"]', '[\"v7f6SIB\"]', '[\"\"]', 0, '2022-12-04 12:37:22', 0, 0, 'số 102 Dương Văn An', '(+84)9322732872', 'member4'),
('Q0Hz4yv', 1, '[\"12\",\"13\"]', '[\"728651.97\",\"1690083.75\"]', '[\"3\",\"1\"]', '[\"YxlGH5N4\",\"GTliKno\"]', '[\"\",\"\"]', 1, '2022-11-30 17:53:03', 0, 1, NULL, NULL, NULL),
('qW5TPws', 12, '[\"10\",\"12\"]', '[\"853312\",\"728651.97\"]', '[\"3\",\"1\"]', '[\"v7f6SIB\",\"YxlGH5N4\"]', '[\"\",\"\"]', 1, '2022-12-04 16:16:06', 0, 1, '1851 Aufderhar Crossing', '(+84)7455098960', 'member6'),
('S9fHQj1', 9, '[\"10\"]', '[\"853312\"]', '[\"2\"]', '[\"v7f6SIB\"]', '[\"\"]', 0, '2022-12-04 10:33:16', 0, 0, '1851 Aufderhar Crossing', '(+84)7455098960', 'Tran Phuc Thinh'),
('sdV8fD6', 9, '[\"10\"]', '[\"853312\"]', '[\"3\"]', '[\"v7f6SIB\"]', '[\"\"]', 0, '2022-12-04 10:28:56', 0, 0, '1851 Aufderhar Crossing', '(+84)7455098960', 'Tran Phuc Thinh'),
('wizPCgh', 9, '[\"32\"]', '[\"671056.62\"]', '[\"2\"]', '[\"dKV7XymHW\"]', '[\"\"]', 0, '2022-12-04 10:50:51', 0, 1, '1851 Aufderhar Crossing', '(+84)7455098960', 'Tran Phuc Thinh'),
('WQUzp75', 8, '[\"7\"]', '[\"1103279\"]', '[\"2\"]', '[\"MnEABiW3\"]', '[\"\"]', 0, '2022-12-04 04:06:39', 1, 1, NULL, NULL, NULL),
('x2hc8He', 8, '[\"13\"]', '[\"1690083.75\"]', '[\"2\"]', '[\"GTliKno\"]', '[\"\"]', 0, '2022-12-03 17:53:19', 1, 1, NULL, NULL, NULL),
('YfsbFmy', 1, '[\"8\",\"12\",\"13\"]', '[\"639977\",\"728651.97\",\"1690083.75\"]', '[\"1\",\"2\",\"1\"]', '[\"V4JujW\",\"YxlGH5N4\",\"GTliKno\"]', '[\"\",\"\",\"\"]', 0, '2022-12-01 20:13:07', 0, 1, NULL, NULL, NULL),
('yg0iqIY', 8, '[\"12\"]', '[\"728651.97\"]', '[\"2\"]', '[\"YxlGH5N4\"]', '[\"\"]', 1, '2022-12-04 04:08:17', 0, 1, NULL, NULL, NULL),
('ygi9keP', 1, '[\"10\",\"12\",\"13\"]', '[\"853312\",\"728651.97\",\"1690083.75\"]', '[\"4\",\"3\",\"1\"]', '[\"v7f6SIB\",\"YxlGH5N4\",\"GTliKno\"]', '[\"\",\"\",\"\"]', 0, '2022-12-04 14:37:29', 0, 1, '1851 Aufderhar Crossing', '(+84)7455098960', 'Admin'),
('yMFdRcE', 9, '[\"10\"]', '[\"853312\"]', '[\"2\"]', '[\"v7f6SIB\"]', '[\"\"]', 0, '2022-12-04 10:36:26', 0, 1, '1851 Aufderhar Crossing', '(+84)7455098960', 'Tran Phuc Thinh'),
('ZqAwC3l', 8, '[\"10\"]', '[\"853312\"]', '[\"1\"]', '[\"v7f6SIB\"]', '[\"\"]', 1, '2022-12-03 15:25:35', 0, 1, NULL, NULL, NULL);

INSERT INTO `category` (`id`, `name`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`) VALUES
(3, 'business', '', '2022-12-02', 1, '2022-12-02', 1, 0, 1);
INSERT INTO `category` (`id`, `name`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`) VALUES
(4, 'History', NULL, '2022-11-23', 1, '2022-11-24', 1, 1, 2);
INSERT INTO `category` (`id`, `name`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`) VALUES
(5, 'Health, Fitness & Dieting', NULL, '2022-11-23', 1, '2022-11-24', 1, 1, 3);
INSERT INTO `category` (`id`, `name`, `picture`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`) VALUES
(6, 'Humor & Entertainment', NULL, '2022-11-23', 1, '2022-11-24', 1, 1, 4),
(7, 'Music', NULL, '2022-11-23', 1, '2022-11-24', 1, 0, 5),
(11, 'Technology', NULL, '2022-11-23', 1, '2022-11-24', 1, 1, 6),
(12, 'Science', '', '2022-11-24', 1, '2022-11-24', 1, 1, 7),
(18, 'Manga', '', '2022-11-24', 1, '2022-11-24', 1, 1, 8),
(19, 'Culture', '', '2022-11-24', 1, '2022-11-24', 1, 1, 9),
(20, 'Education', '', '2022-11-28', 1, '2022-11-28', 1, 1, 11),
(22, 'Biography', '60x90-r1FGT.png', '2022-11-28', 1, '2022-11-28', 1, 1, 76);

INSERT INTO `group` (`id`, `name`, `group_acp`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `privilege_id`) VALUES
(1, 'Admin', 1, '2022-11-25 00:00:00', 1, '2022-12-02 14:12:24', 1, 1, 15, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18');
INSERT INTO `group` (`id`, `name`, `group_acp`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `privilege_id`) VALUES
(2, 'Manager', 1, '2022-11-23 00:00:00', NULL, '2022-12-04 16:12:55', 1, 1, 5, '1,2,3,4,6,7,8,9,10,11,12,13,14,15,17,18');
INSERT INTO `group` (`id`, `name`, `group_acp`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `privilege_id`) VALUES
(3, 'Member', 0, '2022-12-04 00:00:00', 1, '2022-12-04 16:12:44', 1, 1, 4, NULL);

INSERT INTO `pre_order` (`id_pre`, `user_id`, `books`, `prices`, `quantities`) VALUES
(10, 8, '[10]', '[1706624]', '[2]');
INSERT INTO `pre_order` (`id_pre`, `user_id`, `books`, `prices`, `quantities`) VALUES
(11, 1, '[10]', '[1706624]', '[2]');


INSERT INTO `privilege` (`id`, `name`, `module`, `controller`, `action`) VALUES
(1, 'Show list users', 'admin', 'user', 'index');
INSERT INTO `privilege` (`id`, `name`, `module`, `controller`, `action`) VALUES
(2, 'Change status of user', 'admin', 'user', 'status');
INSERT INTO `privilege` (`id`, `name`, `module`, `controller`, `action`) VALUES
(3, 'Update information of user', 'admin', 'user', 'form');
INSERT INTO `privilege` (`id`, `name`, `module`, `controller`, `action`) VALUES
(4, 'Change status of user by ajax', 'admin', 'user', 'ajax_status'),
(5, 'Delete one or more people', 'admin', 'user', 'trash'),
(6, 'Change position of user', 'admin', 'user', 'ordering'),
(7, 'access Admin Control Panel', 'admin', 'index', 'index'),
(8, 'Login Admin Control Panel', 'admin', 'index', 'login'),
(9, 'Logout Admin Control Panel', 'admin', 'index', 'logout'),
(10, 'Update information admin', 'admin', 'index', 'profile'),
(11, 'Show list group', 'admin', 'group', 'index'),
(12, 'Change status of user', 'admin', 'group', 'status'),
(13, 'Update information of group', 'admin', 'group', 'form'),
(14, 'Change status of group by ajax', 'admin', 'group', 'ajax_status'),
(15, 'Change group_acp of group by ajax', 'admin', 'group', 'group_acp_status'),
(16, 'Delete one or more group', 'admin', 'group', 'trash'),
(17, 'Change ordering of user', 'admin', 'user', 'ordering'),
(18, 'Change ordering of group', 'admin', 'group', 'ordering');

INSERT INTO `user` (`id`, `user_name`, `email`, `full_name`, `password`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `register_date`, `register_ip`, `visited_date`, `visited_ip`, `group_id`, `address`, `phone`) VALUES
(1, 'Admin', 'Admin@gmail.com', 'Admin', 'Php123@123', '2022-11-18 00:00:00', NULL, '2022-11-25 11:11:53', 1, 1, 12, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `user` (`id`, `user_name`, `email`, `full_name`, `password`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `register_date`, `register_ip`, `visited_date`, `visited_ip`, `group_id`, `address`, `phone`) VALUES
(2, 'Trinh Chi Cao', 'ghuijjjjj@gmail.com', 'Dang Thanh Binh', 'Php1234@233', '2022-12-02 00:00:00', NULL, '2022-12-02 15:12:51', 1, 1, 23, '2022-11-22 05:11:37', '172.26.0.1', NULL, NULL, 3, NULL, NULL);
INSERT INTO `user` (`id`, `user_name`, `email`, `full_name`, `password`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `register_date`, `register_ip`, `visited_date`, `visited_ip`, `group_id`, `address`, `phone`) VALUES
(3, 'k8osOcuN', 'full@gmail.com', 'Dang Thanh Binh', 'Php123@123', '2022-11-28 00:00:00', 1, '2022-11-28 15:11:24', 1, 0, 12, NULL, NULL, NULL, NULL, 2, NULL, NULL);
INSERT INTO `user` (`id`, `user_name`, `email`, `full_name`, `password`, `created`, `created_by`, `modified`, `modified_by`, `status`, `ordering`, `register_date`, `register_ip`, `visited_date`, `visited_ip`, `group_id`, `address`, `phone`) VALUES
(4, 'Marci', 'Marci@gmail.com', 'Dang Thanh Thao Nhi', 'Php123@123', '2022-11-25 11:11:09', 1, '2022-11-25 00:00:00', NULL, 1, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(5, 'member', 'member@gmail.com', 'Dang Thanh Binh', 'Php123@123', '2022-12-04 00:00:00', 1, '2022-12-04 09:12:53', 1, 1, 6, NULL, NULL, NULL, NULL, 3, '1851 Aufderhar Crossing', '7455098960'),
(6, 'fdfdsfs', 'tuanngsadaoc78919@gmail.com', 'Mirannnaadsad', 'Php1234@233', '2022-12-02 14:12:17', 1, '2022-12-02 00:00:00', NULL, 1, 8, NULL, NULL, NULL, NULL, 2, NULL, NULL),
(7, 'thinh', 'tjijic78919@gmail.com', 'Dang Thanh Thao 855', 'Php123@123', '2022-12-02 00:00:00', NULL, NULL, NULL, 1, NULL, '2022-12-02 15:12:58', '172.22.0.1', NULL, NULL, NULL, NULL, NULL),
(8, 'member1', 'member1@gmail.com', 'Le Trong Tan', 'Php123@123', '2022-12-03 00:00:00', 1, '2022-12-03 15:12:41', 1, 0, 3, NULL, NULL, NULL, NULL, 3, 'k12 dien bien phu, uqan lien chiuey, dn', '0987215654'),
(9, 'member2', 'member2@gmail.com', 'Tran Phuc Thinh', 'Php123@123', '2022-12-04 09:12:38', 1, '2022-12-04 00:00:00', NULL, 1, 4, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(10, 'member3', 'tuanngoc78919@gmail.com', 'Nguyen Tran Phuc', 'Php123@123', '2022-12-04 00:00:00', NULL, NULL, NULL, 1, NULL, '2022-12-04 11:12:07', '172.22.0.1', NULL, NULL, NULL, NULL, NULL),
(11, 'member4', 'member4@gmail.com', 'member4', 'Html123@123', '2022-12-04 00:00:00', NULL, '2022-12-04 13:12:51', 11, 1, NULL, '2022-12-04 11:12:31', '172.22.0.1', NULL, NULL, NULL, 'số 102 Dương Văn An', '9322732872'),
(12, 'member6', 'member67@gmail.com', 'member6', 'Html123@123', '2022-12-04 00:00:00', NULL, '2022-12-04 16:12:18', 12, 1, NULL, '2022-12-04 16:12:35', '172.22.0.1', NULL, NULL, NULL, '1851 Aufderhar Crossing', '7455098960');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;