<?php 
    $link_img = TEMPLATE_LINK_PATH."default/main/".$this->imgs ."/";
    $link_home              = url::create_url("default","index","index");
    $link_categories        = url::create_url("default","category","index");
    $link_my_account        = url::create_url("default","user","index");
    $link_register          = url::create_url("default","index","register");
    $link_login             = url::create_url("default","index","login");
    $link_logout            = url::create_url("default","user","logout");
    $link_acp               = url::create_url("admin","index","index");

?>
<div class="header">
    <div class="logo"><a href="index.html"><img src="<?php echo $link_img?>logo.gif" alt="" title="" border="0" /></a></div>
    <div id="menu">
        <ul>
            <li class="index-index"     ><a href="<?php echo $link_home ?>">Home</a></li>
            <li class="category-index"><a href="<?php echo $link_categories ?>">Category</a></li>
            <?php 
            if((isset($_SESSION["user"]["login"]) && ($_SESSION["user"]["login"] == 1)))
            { 
                echo '<li class="user-index user-change_pass user-cart user-history"><a href="'.$link_my_account.'">My accout</a></li>
                        <li class="user-logout"><a href="'.$link_logout.'">Logout</a></li>';
                if($_SESSION["user"]["group_acp"] == 1)
                {
                    echo '<li class=""><a href="'.$link_acp.'">Admin Control Panel</a></li>';
                }
            }
            else 
            {
                echo '<li class="index-register"><a href="'.$link_register.'">Register</a></li>
                        <li class="index-login"><a href="'.$link_login.'">Login</a></li>';            
            }
            ?>
            
        </ul>
    </div>
</div>