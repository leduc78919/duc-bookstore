<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="UTF-8">
    <?php echo $this->meta_http;
    echo $this->meta_name;
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <?php echo $this->title;
    echo $this->css;
    echo $this->js;
    ?>
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>

</head>

<body>
    <div id="wrap">
        <?php require_once('html/header.php') ?>
        <div class="center_content">
            <div class="left_content">
                <?php require_once($this->view_path); ?>
            </div>
            <!--end of left content-->

            <div class="right_content">
                <?php require_once(BLOCK_PATH . "language.php");
                    require_once(BLOCK_PATH . "cart.php");
                    require_once(BLOCK_PATH . "promotion.php");
                    require_once(BLOCK_PATH . "category.php");
                ?>
            </div>
            <!--end of right content-->




            <div class="clear"></div>
        </div>
        <!--end of center content-->
        <?php require_once('html/footer.php') ?>


    </div>

</body>

</html>