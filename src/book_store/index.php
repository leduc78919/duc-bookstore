<?php
    // require_once('libs/bootstrap.php');
    // require_once('libs/controller.php');
    // require_once('libs/view.php');
    
    
    // autoload
    require_once("define.php");
    function autoload($className)
    {
        require_once LIB_PATH. "{$className}.php";
    }
    spl_autoload_register("autoload");
    Session::init();
    $bootstrap = new bootstrap();
    $bootstrap->init();


?>
