<?php 
class validate {
    private $error = array();
    private $source;
    private $rules;
    private $result = array(
        "name" =>"", "age" =>"", "url" =>"", "email" =>"",
        "ordering" => "", "status" =>"", "special" => "", "password" =>"",
        "first_name" => "", "last_name" => "", "user_name" => "", "address" => "",
        "birthday" => "", "group_acp" => "", "full_name" =>"", "group_id" =>"",
        "file"  => "", "category_id" => "", "price" => "" , "sale_off" => "",
        "description" => "","phone" => "","address" => ""
    );

    public function __construct($source)
    {
        $this->source = $source;
    }

    //add rule
    public function addRule($element,$type,$options = null )
    {
        $this->rules[$element] = array(
            "type" => $type,
            "options" => $options
        );
        return $this;
    }

    //in mang
    public function printM($value)
    {
        echo "<pre>";
        print_r($value);
        echo "</pre>";
    }

    //checkname
    public function checkname($value,$min,$max,$id)
    {
        if(is_string($value))
        {
            $length = strlen($value);
            if($length < $min || $length > $max)
            {
                $this->error["$id"] = "do dai khong hop le";
            }
        }
        else
        {
            $this->error["$id"] = 'ten khong hop le';
        }
    }

    //checkage
    public function checkage($value,$min,$max)
    {
        if(!filter_var($value,FILTER_VALIDATE_INT,array("options" => array("min_range" => $min, "max_range" => $max))))
        {
            $this->error['age'] = 'tuoi khong hop le';
        }
    }

    //check birthday
    public function checkbirthday($value,$start,$end)
    {
        $startList = date_parse_from_format("d/m/Y",$start);
        $startChoose = mktime(0,0,0,$startList["month"],$startList["day"],$startList["year"]);

        $endList = date_parse_from_format("d/m/Y",$end);
        $endChoose = mktime(0,0,0,$endList["month"],$endList["day"],$endList["year"]);

        if($value < $startChoose || $value > $endChoose)
        {
            $this->error['birthday'] = 'ngay sinh khong hop le';
        }
    }

    //check ordering
    public function checkordering($value,$min,$max,$id)
    {
        if(!filter_var($value,FILTER_VALIDATE_INT,array("options" => array("min_range" => $min, "max_range" => $max))))
        {
            $this->error[$id] = $id . ' khong hop le';
        }
    }

    //checkemail
    public function checkemail($value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = 'email khong hop le';
        }
    }

    //checkurl
    public function checkurl($value)
    {
        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            $this->error['url'] = 'url khong hop le';
        }
    }
    // check status
    public function checkstatus($value,$arr_deny,$id)
    {
        if(in_array($value,$arr_deny))
        {
            $this->error[$id] = 'yeu cau chon option';
        }
    }

    //check password
    public function password ($value, $id)
    {
        $pattern = '#^(?=.*\W)(?=.*[A-Z])(?=.*\d).{8,}$#';
        $flag = preg_match($pattern,$value);
        if($flag == 0)
        {
            $this->error["$id"] = 'password ko phu hop';
        }
    }

    // check sale off
    public function check_number($value,$min,$max,$id)
    {
        if($value < $min || $value > $max)
        {
            $this->error["$id"] = $id . ' ko phu hop';
        }
    }

    

    //show error
    public function showError()
    {
       return $this->error;
    }
    //show result
    public function showResult()
    {
        return $this->result;
    }

    // is valid
    public function isValid()
    {
        if(count($this->error) > 0)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    // delete key result no use
    public function deletekeyRe($arr,&$source)
    {
        foreach($arr as $value)
        {
            if(array_key_exists($value,$source))
            {
                unset($source[$value]);
            }
        }
    }

    public function check_exit_record($element,$options) // select 
    {
        $list = $options['database']->single_record($options["query"]);
        if(!empty($list))
        {
            $this->error[$element] = "Gia tri da ton tai";
        }

    }

    // check file size
    public function checksize($fileSize,$minSize,$maxSize)
    {
        $flag = 0;
        if($fileSize >= $minSize && $fileSize <= $maxSize)
        {
            $flag = 1;
        }
        return $flag;
    }

    //check extension
    public function checkExtension($filename,$arr)
    {
        $extension = pathinfo($filename,PATHINFO_EXTENSION);
        if(in_array($extension,$arr))
        {
            return 1;
        }
        return 0;
    }

    //checkfile
    public function check_file($params, $min, $max,$extension)
    {
        if($params["tmp_name"] != null)
        {
            if($this->checksize($params["size"], $min,$max) == 0)
            {
                $this->error["file"] = "size khong dung kich thuoc";
            }
            if($this->checkExtension($params["name"],$newarr = explode("||",$extension)) == 0)
            {
                if(isset($this->error["file"]) && strlen($this->error["file"]) > 0)
                {
                    $this->error["file"] .= " and extension ko phu hop";
                }
                else
                {
                    $this->error["file"] = "extension ko phu hop";
                }
                
            }
        }
        
    }
    //run
    public function run()
    {
        // echo "<pre>";
        // print_r($this->source);
        // echo "</pre>";
        // die();
        foreach($this->rules as $key => $value)
        {
            if((is_array($this->source[$key]) == false) && (trim($this->source[$key]) == null))
            {
                $this->error[$key] = 'khong duoc de rong';
            }
            else 
            {
            switch ($value["type"]) {
                case 'string':
                    $this->checkname($this->source[$key],$value['options']['min'],$value['options']['max'], $value['options']["id"]);
                    break;
                case 'int':
                    $this -> checkage($this->source[$key],$value['options']['min'],$value['options']['max']);
                    break;
                case 'birthday':
                    $this -> checkbirthday($this->source[$key],$value['options']['min'],$value['options']['max']);
                    break;
                case 'numeric':
                    $this -> checkordering($this->source[$key],$value['options']['min'],$value['options']['max'],$value['options']['id']);
                    break;
                case 'status':
                    $this -> checkstatus($this->source[$key],$value['options']["deny"],$value["options"]["id"]);
                    break;
                case 'password':
                    $this -> password($this->source[$key], $value['options']["id"]);
                    break;
                case 'url':
                    $this -> checkurl($this->source[$key]);
                    break;
                case 'email':
                    $this -> checkemail($this->source[$key]);
                    break;
                case 'string-exit':
                    $this->checkname($this->source[$key],$value['options']['min'],$value['options']['max'], $value['options']["id"]);
                    $this->check_exit_record($key,array("database" => $value['options']['database'],"query" => $value['options']['query']));
                    break;
                case 'email-exit':
                    $this -> checkemail($this->source[$key]);
                    $this->check_exit_record($key,array("database" => $value['options']['database'],"query" => $value['options']['query']));
                    break;
                case 'file':
                    $this->check_file($this->source[$key],$value['options']['min'],$value['options']['max'],$value['options']['extension']);
                    break;
                case 'number':
                    $this->check_number($this->source[$key],$value['options']['min'],$value['options']['max'],$value['options']['id']);
                    break;
            }
            }

            if (!(array_key_exists($key, $this->error))) {
                $this->result[$key] = $this->source[$key];
            }
        }


        $valueNotValid = array_diff_key($this->source, $this->error);
        $this->result = array_merge($this->result, $valueNotValid);
    }
}