<?php 
class bootstrap
{
   private $params;
   private $array_params;
   private $module;
   private $controller;
   private $action;
   private $controller_object;
   private $controller_name;
   private $file_path;
   public function init()
   {
      $this->setting();
      $this->load_file();
      
   }

   public function setting()
   {
      $this->params = array_merge($_GET,$_POST);
      if(!empty($this->params) && isset($this->params["module"]) && isset($this->params["controller"]) && isset($this->params["action"]))
      {
         $this->module     = $this->params["module"];
         $this->controller = $this->params["controller"];
         $this->action     = $this->params["action"];

      }
      else
      {
         $this->module     = DEFAULT_MODULE;
         $this->controller = DEFAULT_CONTROLLER;
         $this->action     = DEFAULT_ACTION;

         $this->params["module"] = DEFAULT_MODULE;
         $this->params["controller"] = DEFAULT_CONTROLLER;
         $this->params["action"] = DEFAULT_ACTION;
      }
      $this->array_params = $this->params;
      $this->controller_name = $this->controller."_controller";
      $this->file_path = MODULE_PATH."$this->module/controllers/".$this->controller_name . ".php";
   }
   public function load_file()
   {
      
      if(file_exists($this->file_path))
      {
         require_once($this->file_path);
         $this->controller_object = new $this->controller_name();
         $this->controller_object->init($this->array_params);
        
         $action_name = $this->action."_action";
         if(method_exists($this->controller_object,$action_name))
         {
            // echo "<pre>";
            // print_r($_SESSION);
            // echo "</pre>";
            if($this->module == "admin")
            {
               $page_login = (($this->controller == "index") && ($this->action == "login"))?1:0;
               $request_url = $this->module."-". $this->controller . "-" . $this->action;
               $logged = (isset($_SESSION["user"]["login"]) && ($_SESSION["user"]["login"] == true) && ($_SESSION["user"]["time"] + TIME_LOGIN > time()))?1:0;
               if($logged)
               {
                  if($_SESSION["user"]["group_acp"] % 2 != 0)
                  {
                     $this->controller_object->$action_name();
                     // $user = $_SESSION["user"]["info"];
                     // if(in_array($request_url,$user["privilege"]))
                     // {
                     //    $this->controller_object->$action_name();
                     // }
                     // else
                     // {
                     //    Session::set("message",array("class" => "error","content" => 'You are not permission'));
                     //    helper::redirect($this->module,$this->controller,"index");
                     // }
                  }
                  else
                  {
                     Session::delete("user");
                     helper::redirect("default","index","notice",array("type" => "not_permission"));
                  }

               }
               else
               {
                  Session::delete("user");
                  if($page_login == 1)
                  {
                     $this->controller_object->$action_name();
                  }
                  else
                  {
                     helper::redirect($this->module,"index","login");
                  }
               }
            }
            else if($this->module == "default")
            {
               $page_login = (($this->controller == "user"))?1:0;
               $logged = (isset($_SESSION["user"]["login"]) && ($_SESSION["user"]["login"] == true) && ($_SESSION["user"]["time"] + TIME_LOGIN > time()))?1:0;   
               if($logged == 1)
               {
                  $this->controller_object->$action_name();
               }
               else
               {
                  if($page_login == 1)
                  {
                     helper::redirect("default","index","login");
                  }
                  else
                  {
                     $this->controller_object->$action_name();
                  }
               }
            }
            
         }
         else
         {
            helper::redirect("default","index","notice",array("type" => "not_url"));
         }
      }
      else
      {
         helper::redirect("default","index","notice",array("type" => "not_url"));
      }
   }

   public function _error()
   {
      require_once( MODULE_PATH."default/controllers/error_controller.php");
      $error_controller = new error_controller();
      $error_controller->set_view("default");
      $error_controller->index_action();

   }
}