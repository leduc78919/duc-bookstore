<?php 
class helper
{
    public static function cms_button($arr_button)
    {
        $button_html = "";
        foreach($arr_button as $value)
        { 
            $link_url = url::create_url($value["module"],$value["controller"],$value["action"]);
            if(isset($value["params"]) &&!empty($value))
            {
                $link_url = url::create_url($value["module"],$value["controller"],$value["action"],$value["params"]);
            }
            if($value["type"] == "new")
            {
                $button_html .= '<li class="button" id="'.$value["id"].'">
                            <a class="modal" href="'.$link_url.'"><span class="'.$value["icon"].'"></span>'.$value["name"].'</a>
                            </li>';
            }
            else if($value["type"] == "submit")
            {
                $button_html .= '<li class="button" id="'.$value["id"].'">
                <a class="modal" href= "#" onclick = "javascript:submit_form_(\''.$link_url.'\'); "><span class="'.$value["icon"].'"></span>'.$value["name"].'</a>
                </li>';
            }
        }
        return $button_html;
    }

    public static function format_date($format,$date)
    {
        $result = "";
        if($date != null)
        {
            $result = date($format, strtotime($date));
        }
        return $result;
    }

    public static function cms_status($value,$link,$id)
    {
        $str_status = ($value % 2 == 0)?"unpublish":"publish";
        $status = '<a class = "jgrid" id = "status-'.$id.'" href="javascript:change_status(\''.$link.'\');"><span class ="state '.$str_status.'"></span></a>';
        return $status;
    }

    public static function cms_special($value,$link,$id)
    {
        $str_status = ($value % 2 == 0)?"unpublish":"publish";
        $status = '<a class = "jgrid" id = "special-'.$id.'" href="javascript:change_special(\''.$link.'\');"><span class ="state '.$str_status.'"></span></a>';
        return $status;
    }

    public static function cms_group_acp_status($value,$link,$id)
    {
        $str_status = ($value % 2 == 0)?"unpublish":"publish";
        $status = '<a class = "jgrid" id = "group-acp-'.$id.'" href="javascript:change_group_acp_status(\''.$link.'\');"><span class ="state '.$str_status.'"></span></a>';
        return $status;
    }

    public static function cms_canceled($value,$link,$id)
    {
        $str_status = ($value % 2 == 0)?"unpublish":"publish";
        $status = '<a class = "jgrid" id = "canceled-'.$id.'" href="#"><span class ="state '.$str_status.'"></span></a>';
        return $status;
    }

    public static function cms_confirm($value,$link,$id)
    {
        $str_status = ($value % 2 == 0)?"unpublish":"publish";
        $status = '<a class = "jgrid" id = "confirm-'.$id.'" href="javascript:change_confirm(\''.$link.'\');"><span class ="state '.$str_status.'"></span></a>';
        return $status;
    }

    public static function cms_link_sort($name,$column,$column_post,$order_post)
    {
        /* <a href = "#" onclick = "JOOMla;">
                ID <img src="/joomla" alt= "" >
            </a>
        */
        
        $image = "";
        $order = ($order_post == "DESC")?"ASC":"DESC";       
        if($column == $column_post)
        {
            $image .= TEMPLATE_LINK_PATH . "admin/main/images/admin/sort_".strtolower($order_post).".png";
        }
        $xhtml = '<a href = "#" onclick = "javacript:sort_link(this,\''.$column.'\',\''.$order.'\');">
                    '.$name.'<img src="'.$image.'" alt= "" >
                </a>';
        return $xhtml;

    }

    public static function cms_select_box($name, $class, $array_select, $key_selected,$style = null)
    {
        /* <select name="filter_state" class="inputbox" onchange="">
                                <option value="">- Select Status -</option>
                            </select> */
        $xhtml = '<select name="'.$name.'" class="'.$class.'" style = "'.$style.'">';
        foreach($array_select  as $key => $value)
        {
            if($key == $key_selected && $key != "default" && $key_selected != null )
            {
                $xhtml .= '<option value="'.$key.'" selected = "selected">'.$value.'</option>';
            }
            else
            {
                $xhtml .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        $xhtml .= '</select>';
        return $xhtml;
    }

    // MESSAGE
    public static function cms_message($message)
    {
        $xhtml = "";
        if($message != null)
        {
            $xhtml = '<dl id="system-message">
                        <dt class="'.$message["class"].'">'.ucfirst($message["class"]).'</dt>
                        <dd class="'.$message["class"].' message">
                            <ul>
                                <li>'.$message["content"].'</li>
                            </ul>
                        </dd>
                    </dl>';
        }
        return $xhtml;
    }

    public static function redirect($module,$controller,$action,$params = null)
    {
        $link = url::create_url($module,$controller,$action,$params);
        header("location: $link");
        exit();
    }

    public static function cms_erros($errors)
    {
        $xhtml ="";
        if(!empty($errors))
        {
            $xhtml .= '<div class = "error-public">';
            $xhtml .= '<ul">';
            foreach($errors as $key => $value)
            {
                $key = str_replace("_"," ",$key);
                $xhtml .= '<li>'.ucwords(trim($key)).': '.$value.'</li>';
            }
            $xhtml .= "</ul>";
            $xhtml .= "</div>";
        }
        return $xhtml;
    }
}