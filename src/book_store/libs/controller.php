<?php 
class controller
{
    public $view;
    protected $params;
    protected $template_obj;
    protected $pagination = array();
    public function __construct()
    {
       
    }

    public function init($array_params)
    {
        $this->set_view($array_params["module"]);
        $this->pagination["current_page"] = (isset($array_params["filter_page"]))?($array_params["filter_page"]) : 1;
        $array_params["pagination"] = $this->pagination;
        $this->params = $array_params;
        $this->view->params = $array_params;
        $this->load_model($array_params["module"],$array_params["controller"]);
        $this->set_template($this);
    }

    public function set_template()
    {
        $this->template_obj = new template($this);
    }

    public function set_view($model_name)
    {
        $this->view = new view($model_name);
    }

    public function set_pagination($config)
    {
        $this->pagination["items_per_page"] = $config["items_per_page"];
        $this->pagination["page_range"] = $config["page_range"];
        $this->params["pagination"] = $this->pagination;
        $this->view->params =  $this->params;
    }

    public function load_model($module_name, $model_name)
    {
        $model_name = $model_name."_model";
        $model_path = MODULE_PATH."$module_name/models/".$model_name . ".php";
        if(file_exists($model_path))
        {
            require_once($model_path);
            $this->db = new $model_name ();
        }
       
    }

    // public function redirect($controller = "index", $action = "index")
    // {
    //     header("location: index.php?controller=$controller&action=$action");
    //     exit();
    // }
    
}