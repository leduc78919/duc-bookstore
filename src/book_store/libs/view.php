<?php 
class view 
{
    protected $modul_name;
    private $template_path;
    protected $view_path;
    protected $title;
    protected $meta_http;
    protected $meta_name;
    protected $css;
    protected $js;
    protected $imgs;
    public function __construct($modul_name)
    {
        $this->modul_name = $modul_name;
    }

    public function render($view_name, $flag = true)
    {
        $view_path =  MODULE_PATH. $this->modul_name."/views/".$view_name . ".php";
        if(file_exists($view_path))
        {
           $this->view_path = $view_path;
           if($flag == true)
           {
                if(isset($this->template_path))
                {
                    require_once($this->template_path);
                }
                else
                {
                    require_once($this->view_path);
                }
           }
           else
           {
                require_once($this->view_path);
           }
           
        }
        else
        {
           echo "error load views";
        }
      
    }

    public function set_template($value)
    {
        return $this->template_path = $value;
    }

    public function set_images($value)
    {
        return $this->imgs = $value;
    }

    public function set_title($value)
    {
        return $this->title = $value;
    }
    
    public function set_meta_http($value)
    {
        return $this->meta_http = $value;
    }

    public function set_meta_name($value)
    {
        return $this->meta_name = $value;
    }

    public function set_css($value)
    {
        return $this->css = $value;
    }

    public function set_js($value)
    {
        return $this->js = $value;
    }
    
    public function append_css($array)
    {
        $append_path_css = APPLICATION_LINK_PATH.$this->modul_name."/views/";
        if(!empty($array))
        {
            foreach($array as $value)
            {
                $this->css .= '<link rel="stylesheet" type="text/css" href="'.$append_path_css.$value.'"/>';
            }
        }
        return $this->css;
    }

    public function append_js($array)
    {
        $append_path_js = APPLICATION_LINK_PATH.$this->modul_name."/views/";
        if(!empty($array))
        {
            foreach($array as $value)
            {
                $this->js .= '<script type="text/javascript" src="'.$append_path_js.$value.'"></script>';
            }
        }
        return $this->js;
    }
       
}