<?php 
class Session 
{
    public static function init()
    {
        session_start();
    }
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
    public static function get($key)
    {
        return $_SESSION[$key];
    }
    public static function delete($key)
    {
        if(is_array($key))
        {
            foreach($key as $value)
            {
                if(isset($_SESSION[$value]))
                    {
                        unset($_SESSION[$value]);
                    }
            }
        }
        else
        {
            if(isset($_SESSION[$key]))
                    {
                        unset($_SESSION[$key]);
                    }
        }
       
        
    }
    public static function destroy()
    {
        session_destroy();
    }
}