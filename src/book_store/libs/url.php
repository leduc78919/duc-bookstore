<?php 
class url 
{
    public static function create_url($module,$controller,$action,$params = null)
    {
        $url = "index.php?module=$module&controller=$controller&action=$action";
        if(!empty($params))
        {
            foreach($params as $key => $value)
            {
                $url .= "&$key=$value";
            }
        }
        return $url;
    }
}