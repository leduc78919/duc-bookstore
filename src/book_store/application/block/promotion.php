<div class="right_box">
<div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>bullet4.gif" alt="" title="" /></span>Promotions</div>
<?php
    $query = 'SELECT id,name,picture FROM `'.DB_TABLE_BOOK .'` WHERE status = 1 AND sale_off > 0 LIMIT 0,3';
    $model = new Model_db();
    $items = $model->list_record($query);
    $xhtml = "";
    if(!empty($items))
    {
        foreach($items as $key => $value)
        {
            $link = url::create_url("default","book","detail",array("book_id" => $value["id"]));
            $picture = (!empty($value["picture"]))?$value["picture"]:"default_img.jpg";
            $image_path = TEMPLATE_FILE_PATH."book/".$picture;
            $xhtml .= '<div class="new_prod_box">
                            <a style="color:black; "  href="'.$link.'">'.$value["name"].'</a>
                            <div class="new_prod_bg">
                                <span class="new_icon"><img src="'.$link_img.'sale_off.png" alt="" title="" /></span>
                                <a href="'.$link.'"><img src="'.$image_path.'" alt="" title="" class="thumb" border="0" /></a>
                            </div>
                        </div>';
        }
    }
  echo $xhtml;  
?>
</div>