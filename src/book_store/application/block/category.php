<?php
    $query = 'SELECT id,name FROM `'.DB_TABLE_CATEGORY .'`';
    $model = new Model_db();
    $items = $model->fetch_pairs($query);
    $xhtml = "";
    if(isset($this->params["book_id"]))
    {
        $query = 'SELECT category_id FROM `'.DB_TABLE_BOOK .'` WHERE id ='.$this->params["book_id"];
        $category_recent = $model->single_record($query);
        $category_choose = $category_recent["category_id"];

    }
    if(!empty($items))
    {
        foreach($items as $key => $value)
        {
            $link = url::create_url("default","book","list",array("category_id" => $key));
            if((isset($this->name_category) && ($this->name_category == $value)) || (isset($category_choose) && ($category_choose == $key)) )
            {
                $xhtml .= '<li><a style = "text-decoration:underline;text-decoration-style: solid;text-decoration-thickness: 1.2px;" href="'.$link.'">'.$value.'</a></li>';
            }
            else
            {
                $xhtml .= '<li><a href="'.$link.'">'.$value.'</a></li>';
            }
            
        }
    }
    
?>
<div class="right_box">

    <div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>bullet5.gif" alt="" title="" /></span>Categories</div>

    <ul class="list">
        <?php echo $xhtml; ?>
    </ul>
</div>