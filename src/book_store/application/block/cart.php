<?php 
    $total = 0;
    $price = 0;
    if(!empty($_SESSION["cart"]))
    {
        $total = array_sum($_SESSION["cart"]["quantity"]);
        $price = array_sum($_SESSION["cart"]["price"]);
    }
    $link = url::create_url("default","user","cart");
?>
<div class="cart">
    <div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>cart.gif" alt="" title="" /></span>My cart</div>
    <div class="home_cart_content">
        <?php echo $total;?> x items | <span class="red">TOTAL: <?php echo number_format($price); ?> VND</span>
    </div>
    <a href="<?php echo $link; ?>" class="view_cart">view cart</a>

</div>