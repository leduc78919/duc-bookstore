<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class cart_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_CART;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE user_id > 0';
       
        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND id LIKE \'%'.$search.'%\'';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) && $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND status = '.$filter_state.'';
            
        }

        // SORT CANCELED
        if(isset($array["filter_cancel"]) && $array["filter_cancel"] != "default")
        {
            $filter_cancel = $array["filter_cancel"];
            $query[] .= 'AND canceled = '.$filter_cancel.'';
            
        }

        // SORT CONFIRMED
        if(isset($array["filter_confirm"]) && $array["filter_confirm"] != "default")
        {
            $filter_confirm = $array["filter_confirm"];
            $query[] .= 'AND confirmed = '.$filter_confirm.'';
            
        }
        // echo $query = implode(" ",$query);
        // echo "<br/>";
        $result = $this->single_record($query);
        return $result["total"];
    }

    public function get_items($array,$options = null)
    {
        $query[] = 'SELECT *';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE user_id > 0';

        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND id LIKE \'%'.$search.'%\'';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) &&  $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND status = '.$filter_state.'';
        }

        //SORT CANCELED
        if(isset($array["filter_cancel"]) && $array["filter_cancel"] != "default")
        {
            $filter_cancel = $array["filter_cancel"];
            $query[] .= 'AND canceled = '.$filter_cancel.'';
            
        }

        // SORT CONFIRMED
        if(isset($array["filter_confirm"]) && $array["filter_confirm"] != "default")
        {
            $filter_confirm = $array["filter_confirm"];
            $query[] .= 'AND confirmed = '.$filter_confirm.'';
            
        }
        //SORT
        if(!empty($array["filter_column"]) && !empty($array["filter_column_dir"]))
        {
            $column = $array["filter_column"];
            $column_dir = $array["filter_column_dir"];
            $query[] = 'ORDER BY '.$column. ' '. $column_dir;
        }
        else
        {
            $query[] = "ORDER BY date DESC";
        }
       
        //PAGINATION
        $pagination = $array["pagination"];
        $items_per_page = $pagination["items_per_page"];
        if($items_per_page > 0)
        {
            $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
            $query[] .= "LIMIT $position,$items_per_page";
        }
        //$query = implode(" ",$query);
        $result = $this->list_record($query);
        return $result;
    }

    public function update_status($params, $options)
    {
        if($options["task"] == "change_ajax_confirm")
        {
            $id = $params["id"];
            $status = 1;
            $query = "UPDATE `".DB_TABLE_CART."` SET confirmed = $status  WHERE id = '$id' ";
            $this->query($query);
            return array($id, $status,"#");
        }        
    }

    public function ordering($params,$options = null)
    {
        $ordering_list = $params["order"];
        $i = 0;
        foreach($ordering_list as $key => $value)
        {
            if(!is_numeric($value))
            {
                Session::set("message",array("class" => "error","content" => 'ordering must be a numeric at id = '. $key));
                helper::redirect("admin","category","index");
            }
            $query = "SELECT * FROM `category` WHERE id = $key AND ordering = $value";
            $single = $this->single_record($query);
            if(empty($single))
            {
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query = "UPDATE `category` SET ordering = $value,modified = '$modified', modified_by = $modified_by  WHERE id = $key";
                $this->query($query);
                $i += 1;
            }            
        }
        if($i > 0)
        {
            Session::set("message",array("class" => "success","content" => $i.' rows changed ordering success!'));
        }
        else
        {
            Session::set("message",array("class" => "error","content" => 'please choose row to change ordering'));
        }
    }

    public function delete_status($params, $options = null)
    {
        if (isset($params["cid"]) && !empty($params["cid"])) 
        {
            $id_in = "(";
            foreach ($params["cid"] as $value) 
            {
                $id_in .= $value . ",";
            }
            $id_in .= "0)";

            //remove_image
            $query_img[] = "SELECT id, picture AS name";
            $query_img[] = "FROM `category` WHERE id IN  $id_in";
            $delete_img = $this->fetch_pairs($query_img);
            foreach ($delete_img as $value) 
            {
                $this->remove_file($options["folder_file"],$value);
            }
            $query[] = "DELETE FROM `category` WHERE id IN $id_in";
            $this->query($query);
            Session::set("message", array("class" => "success", "content" => $this->affected_rows() . ' deleted!'));
        } 
        else 
        {
            Session::set("message", array("class" => "error", "content" => 'please choose row to delete'));
        }
    }

    public function remove_file($folder,$value)
    {
        if ($value != null) {
            unlink(TEMPLATE_FILE_PATH . $folder . $value);
            $value = str_replace("60x90-","",$value);
            unlink(TEMPLATE_FILE_PATH . $folder . $value);
        }
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "add")
        {
            $picture = "";
            if(isset($params["file"]))
            {
                require_once(TEMPLATE_LIB_EXTEND_PATH . "upload.php");
                $obj_upload = new upload();
                $picture = $obj_upload->upload_file($params["file"],"category");
            }
            unset($params["file"]);
            $params["picture"] = $picture;
            $this->insert_db($params);
            Session::set("message",array("class" => "success","content" => 'Data is added success!'));
            return $this->last_id();
            
        }
        else if($options["task"] == "edit")
        {
           
            if(isset($params["file"]))
            {
                require_once(TEMPLATE_LIB_EXTEND_PATH . "upload.php");
                $obj_upload = new upload();
                $this->remove_file("category/",$params["picture"]);
                $picture = $obj_upload->upload_file($params["file"],"category");    
                unset($params["picture"]);
                unset($params["file"]);
                $params["picture"] = $picture;
            } 
            $where = array(array("id",$options["id"]));
            $this->update_db($params,$where);
            Session::set("message",array("class" => "success","content" => 'Data is edited success!'));
            return $options["id"];
        }
    }

    public function single_item($params,$options = null)
    {
        $query[] = 'SELECT name,status,ordering,picture';
        $query[] = 'FROM `'.$this->table .'`';
        $query[] = 'WHERE id ='. $params["id"];
        $result = $this->single_record($query);
        return $result; 
    }
}