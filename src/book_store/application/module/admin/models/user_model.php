<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class user_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_USER;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0';

        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND (user_name LIKE \'%'.$search.'%\' OR email LIKE \'%'.$search.'%\' OR full_name LIKE \'%'.$search.'%\')';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) && $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND status = '.$filter_state.'';
            
        }

        // SORT GROUP 
        if(isset($array["filter_group_id"]) &&  $array["filter_group_id"] != "default")
        {
            $filter_group_state = $array["filter_group_id"];
            $query[] .= 'AND group_id = '.$filter_group_state.'';
        }

        // echo $query = implode(" ",$query);
        // echo "<br/>";
        $result = $this->single_record($query);
        return $result["total"];
    }

    public function get_items($array,$options = null)
    {
    //     echo "<pre>";
    //     print_r($array);
    //     echo "</pre>";
        $query[] = 'SELECT u.id,u.user_name,u.email,u.full_name,u.status,u.ordering,u.created,
                        u.created_by,u.modified,u.modified_by,u.phone,u.address,g.name as group_name';
        $query[] = 'FROM `'.$this->table.'` as u LEFT JOIN `'.DB_TABLE_GROUP.'` as g';
        $query[] = 'ON u.group_id = g.id WHERE u.id > 0';

        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND (u.user_name LIKE \'%'.$search.'%\'OR u.email LIKE \'%'.$search.'%\' OR u.full_name LIKE \'%'.$search.'%\')';
        }

        // SORT STATUS
        if(isset($array["filter_state"]) &&  $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND u.status = '.$filter_state.'';
        }

        // SORT GROUP 
        if(isset($array["filter_group_id"]) &&  $array["filter_group_id"] != "default")
        {
            $filter_group_state = $array["filter_group_id"];
            $query[] .= 'AND u.group_id = '.$filter_group_state.'';
        }

        //SORT
        if(!empty($array["filter_column"]) && !empty($array["filter_column_dir"]))
        {
            $column = $array["filter_column"];
            $column_dir = $array["filter_column_dir"];
            $query[] = 'ORDER BY '.$column. ' '. $column_dir;
        }
        
        //PAGINATION
        $pagination = $array["pagination"];
        $items_per_page = $pagination["items_per_page"];
        if($items_per_page > 0)
        {
            $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
            $query[] .= "LIMIT $position,$items_per_page";
        }
        // echo $query = implode(" ",$query);
        $result = $this->list_record($query);
        if(empty($result))
        {
            Session::set("message",array("class" => "error","content" => 'No User in class there'));
        }
        return $result;
    }

    public function update_status($params, $options)
    {
        if($options["task"] == "change_ajax_status")
        {
            $modified      = date("Y-m-d H:m:s",time());
            $modified_by  = (int)$_SESSION["user"]["info"]["id"];
            $id = $params["id"];
            $status = ($params["status"] == 0)? 1 : 0;
            $query = "UPDATE `user` SET status = $status,modified = '$modified', modified_by = $modified_by WHERE id = $id ";
            $this->query($query);
            return array($id, $status,url::create_url("admin","user","ajax_status",array("id" => $id, "status" => $status)));
        }

        if($options["task"] == "change_all_status")
        {
            $type = $params["type"];
            if(isset($params["cid"]) && !empty($params["cid"]))
            {
                 $id_in = "(";
                foreach($params["cid"] as $value)
                {
                    $id_in .= $value . ","; 
                }
                $id_in .= "0)";
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query[] = "UPDATE `user` SET status = $type,modified = '$modified', modified_by = $modified_by WHERE id IN $id_in";
                $this->query($query);
                Session::set("message",array("class" => "success","content" => 'update '.$this->affected_rows().' rows changed success!'));
            }
            else
            {
                Session::set("message",array("class" => "error","content" => 'please choose row to update'));
            }
           
        }
        
    }

    public function ordering($params,$options = null)
    {
        $ordering_list = $params["order"];
        $i = 0;
        foreach($ordering_list as $key => $value)
        {
            if(!is_numeric($value))
            {
                Session::set("message",array("class" => "error","content" => 'ordering must be a numeric at id = '. $key));
                helper::redirect("admin","user","index");
            }
            $query = "SELECT * FROM `user` WHERE id = $key AND ordering = $value";
            $single = $this->single_record($query);
            if(empty($single))
            {
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query = "UPDATE `user` SET ordering = $value,modified = '$modified', modified_by = $modified_by WHERE id = $key";
                $this->query($query);
                $i += 1;
            }            
        }
        if($i > 0)
        {
            Session::set("message",array("class" => "success","content" => $i.' rows changed ordering success!'));
        }
        else
        {
            Session::set("message",array("class" => "error","content" => 'please choose row to change ordering'));
        }
    }

    public function delete_status($params, $options = null)
    {
        if($options == null)
        {
            if(isset($params["cid"]) && !empty($params["cid"]))
            {
                 $id_in = "(";
                foreach($params["cid"] as $value)
                {
                    $id_in .= $value . ","; 
                }
                $id_in .= "0)";
                $query[] = "DELETE FROM `user` WHERE id IN $id_in";
                $this->query($query);
                Session::set("message",array("class" => "success","content" => $this->affected_rows().' deleted!'));
            }
            else
            {
                Session::set("message",array("class" => "error","content" => 'please choose row to delete'));
            }
        }
        
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "add")
        {
            $this->insert_db($params);
            Session::set("message",array("class" => "success","content" => 'Data is added success!'));
            return $this->last_id();
            
        }
        else if($options["task"] == "edit")
        {
           $where = array(array("id",$options["id"]));
           $this->update_db($params,$where);
           Session::set("message",array("class" => "success","content" => 'Data is edited success!'));
           return $options["id"];
        }
    }

    public function single_item($params,$options = null)
    {
        $query[] = 'SELECT u.id,u.user_name,u.email,u.full_name,u.status,u.ordering,u.password,u.created,
                        u.created_by,u.modified,u.modified_by,u.phone,u.address,g.id as group_id';
        $query[] = 'FROM `'.$this->table.'` as u,`'.DB_TABLE_GROUP.'` as g';
        $query[] = 'WHERE u.group_id = g.id AND u.id ='. $params["id"].'';
        $result = $this->single_record($query);
        return $result; 
    }

    public function item_in_box($params,$options = null)
    {
        $query[] = 'SELECT id,name';
        $query[] = 'FROM `'.DB_TABLE_GROUP.'`';
        $result = $this->fetch_pairs($query);
        return $result;
    }
}