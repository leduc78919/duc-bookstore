<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class book_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_BOOK;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0';
       
        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND name LIKE \'%'.$search.'%\'';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) && $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND status = '.$filter_state.'';
            
        }

        // SORT SPECIAL
        if(isset($array["filter_special"]) && $array["filter_special"] != "default")
        {
            $filter_special = $array["filter_special"];
            $query[] .= 'AND special = '.$filter_special.'';
            
        }
        // echo $query = implode(" ",$query);
        // echo "<br/>";
        $result = $this->single_record($query);
        return $result["total"];
    }

    public function get_items($array,$options = null)
    {
        $query[] = 'SELECT b.id,b.name,b.description,b.price,b.special,b.sale_off,b.picture,b.created,b.created_by,
                    b.modified,b.modified_by,b.status,b.ordering,c.name as category_name';
        $query[] = 'FROM `'.$this->table.'` AS b LEFT JOIN `'.DB_TABLE_CATEGORY.'` AS c';
        $query[] = 'ON b.category_id = c.id WHERE b.id > 0';

        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND b.name LIKE \'%'.$search.'%\'';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) &&  $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND b.status = '.$filter_state.'';
        }

        // SORT SPECIAL
        if(isset($array["filter_special"]) &&  $array["filter_special"] != "default")
        {
            $filter_special = $array["filter_special"];
            $query[] .= 'AND b.special = '.$filter_special.'';
        }

        // SORT CATEGORY
        if(isset($array["filter_group_id"]) &&  $array["filter_group_id"] != "default")
        {
            $filter_category_state = $array["filter_group_id"];
            $query[] .= 'AND b.category_id = '.$filter_category_state.'';
        }

        //SORT
        if(!empty($array["filter_column"]) && !empty($array["filter_column_dir"]))
        {
            $column = $array["filter_column"];
            $column_dir = $array["filter_column_dir"];
            $query[] = 'ORDER BY '.$column. ' '. $column_dir;
        }
        
        //PAGINATION
        $pagination = $array["pagination"];
        $items_per_page = $pagination["items_per_page"];
        if($items_per_page > 0)
        {
            $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
            $query[] .= "LIMIT $position,$items_per_page";
        }
        // echo $query = implode(" ",$query);
        $result = $this->list_record($query);
        return $result;
    }

    public function update_status($params, $options)
    {
        if($options["task"] == "change_ajax_status")
        {
            $modified      = date("Y-m-d H:m:s",time());
            $modified_by  = (int)$_SESSION["user"]["info"]["id"];
            $id = $params["id"];
            $status = ($params["status"] == 0)? 1 : 0;
            $query = "UPDATE `".DB_TABLE_BOOK."` SET status = $status ,modified = '$modified', modified_by = $modified_by WHERE id = $id ";
            $this->query($query);
            return array($id, $status,url::create_url("admin","book","ajax_status",array("id" => $id, "status" => $status)));
        }

        if($options["task"] == "change_ajax_special")
        {
            $modified      = date("Y-m-d H:m:s",time());
            $modified_by  = (int)$_SESSION["user"]["info"]["id"];
            $id = $params["id"];
            $special = ($params["special"] == 0)? 1 : 0;
            $query = "UPDATE `".DB_TABLE_BOOK."` SET special = $special ,modified = '$modified', modified_by = $modified_by WHERE id = $id ";
            $this->query($query);
            return array($id, $special,url::create_url("admin","book","ajax_special",array("id" => $id, "special" => $special)));
        }

        if($options["task"] == "change_all_status")
        {
            $type = $params["type"];
            if(isset($params["cid"]) && !empty($params["cid"]))
            {
                 $id_in = "(";
                foreach($params["cid"] as $value)
                {
                    $id_in .= $value . ","; 
                }
                $id_in .= "0)";
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query[] = "UPDATE `".DB_TABLE_BOOK."` SET status = $type,modified = '$modified', modified_by = $modified_by  WHERE id IN $id_in";
                $this->query($query);
                Session::set("message",array("class" => "success","content" => 'update '.$this->affected_rows().' rows changed success!'));
            }
            else
            {
                Session::set("message",array("class" => "error","content" => 'please choose row to update'));
            }
           
        }
        
    }

    public function ordering($params,$options = null)
    {
        $ordering_list = $params["order"];
        $i = 0;
        foreach($ordering_list as $key => $value)
        {
            if(!is_numeric($value))
            {
                Session::set("message",array("class" => "error","content" => 'ordering must be a numeric at id = '. $key));
                helper::redirect("admin","book","index");
            }
            $query = "SELECT * FROM `".DB_TABLE_BOOK."` WHERE id = $key AND ordering = $value";
            $single = $this->single_record($query);
            if(empty($single))
            {
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query = "UPDATE `".DB_TABLE_BOOK."` SET ordering = $value,modified = '$modified', modified_by = $modified_by  WHERE id = $key";
                $this->query($query);
                $i += 1;
            }            
        }
        if($i > 0)
        {
            Session::set("message",array("class" => "success","content" => $i.' rows changed ordering success!'));
        }
        else
        {
            Session::set("message",array("class" => "error","content" => 'please choose row to change ordering'));
        }
    }

    public function delete_status($params, $options = null)
    {
        if (isset($params["cid"]) && !empty($params["cid"])) 
        {
            $id_in = "(";
            foreach ($params["cid"] as $value) 
            {
                $id_in .= $value . ",";
            }
            $id_in .= "0)";

            //remove_image
            $query_img[] = "SELECT id, picture AS name";
            $query_img[] = "FROM `".DB_TABLE_BOOK."` WHERE id IN  $id_in";
            $delete_img = $this->fetch_pairs($query_img);
            foreach ($delete_img as $value) 
            {
                $this->remove_file($options["folder_file"],$value);
            }
            $query[] = "DELETE FROM `".DB_TABLE_BOOK."` WHERE id IN $id_in";
            $this->query($query);
            Session::set("message", array("class" => "success", "content" => $this->affected_rows() . ' deleted!'));
        } 
        else 
        {
            Session::set("message", array("class" => "error", "content" => 'please choose row to delete'));
        }
    }

    public function remove_file($folder,$value)
    {
        if ($value != null) {
            unlink(TEMPLATE_FILE_PATH . $folder . $value);
            $value = str_replace("60x90-","",$value);
            unlink(TEMPLATE_FILE_PATH . $folder . $value);
        }
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "add")
        {
            $picture = "";
            if(isset($params["file"]))
            {
                require_once(TEMPLATE_LIB_EXTEND_PATH . "upload.php");
                $obj_upload = new upload();
                $picture = $obj_upload->upload_file($params["file"],"book");
            }
            unset($params["file"]);
            $params["picture"] = $picture;
            $params["description"] = mysqli_real_escape_string($this->conn,$params["description"]);
            $this->insert_db($params);
            Session::set("message",array("class" => "success","content" => 'Data is added success!'));
            return $this->last_id();
            
        }
        else if($options["task"] == "edit")
        {
            if(isset($params["file"]))
            {
                require_once(TEMPLATE_LIB_EXTEND_PATH . "upload.php");
                $obj_upload = new upload();
                $this->remove_file("book/",$params["picture"]);
                $picture = $obj_upload->upload_file($params["file"],"book");    
                unset($params["picture"]);
                unset($params["file"]);
                $params["description"] = mysqli_real_escape_string($this->conn,$params["description"]);
                $params["picture"] = $picture;
            } 
            $where = array(array("id",$options["id"]));
            $this->update_db($params,$where);
            Session::set("message",array("class" => "success","content" => 'Data is edited success!'));
            return $options["id"];
        }
    }

    public function single_item($params,$options = null)
    {
        $query[] = 'SELECT id,name,description,price,special,sale_off,picture,created,created_by,
                    modified,modified_by,status,ordering,category_id';
        $query[] = 'FROM `'.$this->table .'`';
        $query[] = 'WHERE id ='. $params["id"];
        $result = $this->single_record($query);
        return $result; 
    }

    public function item_in_box($params,$options = null)
    {
        $query[] = 'SELECT id,name';
        $query[] = 'FROM `'.DB_TABLE_CATEGORY.'`';
        $result = $this->fetch_pairs($query);
        return $result;
    }
}