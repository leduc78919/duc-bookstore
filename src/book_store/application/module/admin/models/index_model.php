<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class index_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_USER;
    }
    public function single_item($params,$options = null)
    {
        $query[] = 'SELECT u.id,u.user_name,u.email,u.full_name,g.id as group_id,g.group_acp,u.password,u.status,u.phone,u.address,g.privilege_id ';
        $query[] = 'FROM `'.$this->table.'` as u LEFT JOIN `'.DB_TABLE_GROUP.'` as g';
        $query[] = 'ON u.group_id = g.id';
        $query[] = 'WHERE user_name = "'.$params["user_name"].'" AND password ="'.$params["password"].'"';
        // echo implode(" ",$query);
        $result = $this->single_record($query);
        if($result["group_acp"] == 1)
        {
            $query_privilege[] = 'SELECT id, CONCAT(module,"-",controller,"-",action) AS name';
            $query_privilege[] = 'FROM `'.DB_TABLE_PRIVILEGE.'`';
            $query_privilege[] = 'WHERE ID IN('.$result["privilege_id"].')';
            $temp = $this->fetch_pairs($query_privilege);
            $result["privilege"] = $temp;
        }
        return $result; 
    }

    public function item_in_box($params,$options = null)
    {
        $query[] = 'SELECT id,name';
        $query[] = 'FROM `'.DB_TABLE_GROUP.'`';
        $result = $this->fetch_pairs($query);
        return $result;
    }

    public function check_login($options)
    {
        $query    = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE user_name = "'.$options['user_name'].'" AND password = "'.$options['password'].'"';
        $list = $this->single_record($query);
        return $list;
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "edit")
        {
           $where = array(array("id",$options["id"]));
           $this->update_db($params,$where);
           Session::set("message",array("class" => "success","content" => 'Data is edited success!'));
           return $options["id"];
        }

        if($options["task"] == "save_pre_order")
        {
            $user_id = $params["user_id"];
            $check_id = $this->check_id_pre_order();
            $books = json_encode($params["books"]);
            $prices = json_encode($params["prices"]);
            $quantities = json_encode($params["quantities"]);
            if(in_array($user_id,$check_id))
            {
                $query = "UPDATE `".DB_TABLE_PRE_ORDER."` SET books = '$books',prices ='$prices',quantities = '$quantities'
                            WHERE user_id = $user_id";
                            
            }
            else
            {
                $query = "INSERT INTO `".DB_TABLE_PRE_ORDER."`(user_id,books,prices,quantities) 
                    VALUES($user_id,'$books','$prices','$quantities')";
            }
            $this->query($query);
            Session::delete("cart");
        }
    }

    public function save_session_after_edit($id)
    {
        $query_session[] = 'SELECT u.id,u.user_name,u.email,u.full_name,g.id as group_id,g.group_acp,u.password,u.status ';
        $query_session[] = 'FROM `'.$this->table.'` as u LEFT JOIN `'.DB_TABLE_GROUP.'` as g';
        $query_session[] = 'ON u.group_id = g.id';
        $query_session[] = 'WHERE u.id = '. $id;
        $result = $this->single_record($query_session);
        return $result; 
    }

    public function check_id_pre_order()
    {
        $query[] = "SELECT user_id";
        $query[] = "FROM `".DB_TABLE_PRE_ORDER."`";
        $result = $this->single_record($query);
        return $result;
    }

}