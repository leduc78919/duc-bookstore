<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class group_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_GROUP;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0';
       
        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND name LIKE \'%'.$search.'%\'';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) && $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND status = '.$filter_state.'';
            
        }

        //SORT GROUP ACP
        if(isset($array["filter_group_acp"]) && $array["filter_group_acp"] != "default")
        {
            $filter_group_acp = $array["filter_group_acp"];
            $query[] .= 'AND group_acp = '.$filter_group_acp.'';
            
        }
        // echo $query = implode(" ",$query);
        // echo "<br/>";
        $result = $this->single_record($query);
        return $result["total"];
    }

    public function get_items($array,$options = null)
    {
        $query[] = 'SELECT *';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0';

        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND name LIKE \'%'.$search.'%\'';
            
        }

        // SORT STATUS
        if(isset($array["filter_state"]) &&  $array["filter_state"] != "default")
        {
            $filter_state = $array["filter_state"];
            $query[] .= 'AND status = '.$filter_state.'';
        }

        //SORT GROUP ACP
        if(isset($array["filter_group_acp"]) && $array["filter_group_acp"] != "default")
        {
            $filter_group_acp = $array["filter_group_acp"];
            $query[] .= 'AND group_acp = '.$filter_group_acp.'';
        }

        //SORT
        if(!empty($array["filter_column"]) && !empty($array["filter_column_dir"]))
        {
            $column = $array["filter_column"];
            $column_dir = $array["filter_column_dir"];
            $query[] = 'ORDER BY '.$column. ' '. $column_dir;
        }
        
        //PAGINATION
        $pagination = $array["pagination"];
        $items_per_page = $pagination["items_per_page"];
        if($items_per_page > 0)
        {
            $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
            $query[] .= "LIMIT $position,$items_per_page";
        }
        // echo $query = implode(" ",$query);
        $result = $this->list_record($query);
        return $result;
    }

    public function update_status($params, $options)
    {
        if($options["task"] == "change_ajax_status")
        {
            $modified      = date("Y-m-d H:m:s",time());
            $modified_by  = (int)$_SESSION["user"]["info"]["id"];
            $id = $params["id"];
            $status = ($params["status"] == 0)? 1 : 0;
            $query = "UPDATE `group` SET status = $status,modified = '$modified', modified_by = $modified_by WHERE id = $id ";
            $this->query($query);
            return array($id, $status,url::create_url("admin","group","ajax_status",array("id" => $id, "status" => $status)));
        }

        if($options["task"] == "change_group_acp_status")
        {
            $modified      = date("Y-m-d H:m:s",time());
            $modified_by  = (int)$_SESSION["user"]["info"]["id"];
            $id = $params["id"];
            $status = ($params["group_acp"]  %2 == 0)? 1 : 0;
            $query = "UPDATE `group` SET group_acp = $status,modified = '$modified', modified_by = $modified_by WHERE id = $id ";
            $this->query($query);
            return array($id, $status,url::create_url("admin","group","group_acp_status",array("id" => $id, "group_acp" => $status)));
        }

        if($options["task"] == "change_all_status")
        {
            $type = $params["type"];
           
            if(isset($params["cid"]) && !empty($params["cid"]))
            {
                 $id_in = "(";
                foreach($params["cid"] as $value)
                {
                    $id_in .= $value . ","; 
                }
                $id_in .= "0)"; 
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query[] = "UPDATE `group` SET status = $type,modified = '$modified', modified_by = $modified_by WHERE id IN $id_in";
                $this->query($query);
                Session::set("message",array("class" => "success","content" => 'update '.$this->affected_rows().' rows changed success!'));
            }
            else
            {
                Session::set("message",array("class" => "error","content" => 'please choose row to update'));
            }
           
        }
        
    }

    public function ordering($params,$options = null)
    {
        $ordering_list = $params["order"];
        $i = 0;
        foreach($ordering_list as $key => $value)
        {
            if(!is_numeric($value))
            {
                Session::set("message",array("class" => "error","content" => 'ordering must be a numeric at id = '. $key));
                helper::redirect("admin","group","index");
            }
            $query = "SELECT * FROM `group` WHERE id = $key AND ordering = $value";
            $single = $this->single_record($query);
            if(empty($single))
            {
                $modified      = date("Y-m-d H:m:s",time());
                $modified_by  = (int)$_SESSION["user"]["info"]["id"];
                $query = "UPDATE `group` SET ordering = $value,modified = '$modified', modified_by = $modified_by WHERE id = $key";
                $this->query($query);
                $i += 1;
            }            
        }
        if($i > 0)
        {
            Session::set("message",array("class" => "success","content" => $i.' rows changed ordering success!'));
        }
        else
        {
            Session::set("message",array("class" => "error","content" => 'please choose row to change ordering'));
        }
    }

    public function delete_status($params, $options = null)
    {
        if($options == null)
        {
            if(isset($params["cid"]) && !empty($params["cid"]))
            {
                 $id_in = "(";
                foreach($params["cid"] as $value)
                {
                    $id_in .= $value . ","; 
                }
                $id_in .= "0)";
                $query[] = "DELETE FROM `group` WHERE id IN $id_in";
                $this->query($query);
                Session::set("message",array("class" => "success","content" => $this->affected_rows().' deleted!'));
            }
            else
            {
                Session::set("message",array("class" => "error","content" => 'please choose row to delete'));
            }
        }
        
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "add")
        {
            $this->insert_db($params);
            Session::set("message",array("class" => "success","content" => 'Data is added success!'));
            return $this->last_id();
            
        }
        else if($options["task"] == "edit")
        {
           $where = array(array("id",$options["id"]));
           $this->update_db($params,$where);
           Session::set("message",array("class" => "success","content" => 'Data is edited success!'));
           return $options["id"];
        }
    }

    public function single_item($params,$options = null)
    {
        $query[] = 'SELECT name,status,group_acp,ordering';
        $query[] = 'FROM `'.$this->table .'`';
        $query[] = 'WHERE id ='. $params["id"];
        $result = $this->single_record($query);
        return $result; 
    }
}