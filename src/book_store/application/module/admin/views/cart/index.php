<?php require_once(MODULE_PATH."admin/views/toolbar.php"); ?>
<?php require_once("submenu/submenu.php"); ?>
<?php 
    // echo "<pre>";
    // print_r($this->items);
    // echo "</pre>";
    $column_post = "";
    $column_post_dir = "";
    if(isset($this->params["filter_column"]) && isset($this->params["filter_column_dir"]))
    {
        $column_post = $this->params["filter_column"];
        $column_post_dir = $this->params["filter_column_dir"];
    }
    $lb_name        = helper::cms_link_sort("User ID","user_id",$column_post,$column_post_dir);
    $lb_id          = helper::cms_link_sort("ID","id",$column_post,$column_post_dir);
   
    $lb_created     = helper::cms_link_sort("Created","date",$column_post,$column_post_dir);
    
    $lb_address     = helper::cms_link_sort("Address","address",$column_post,$column_post_dir);
    $lb_phone       = helper::cms_link_sort("Phone","phone",$column_post,$column_post_dir);
    $lb_order_name  = helper::cms_link_sort("Receiver","order_person",$column_post,$column_post_dir);
    

   
    $pagination_html =  $this->pagination->create_html(url::create_url("admin","cart","index"));
    //SELECT BOX
    $status_box    = helper::cms_select_box("filter_state","inputbox",array("default" => "select status","1" => "publish", "0" => "unpublish"),@$this->params["filter_state"]);    //MESSAGE
    $cancel_box    = helper::cms_select_box("filter_cancel","inputbox",array("default" => "select canceled order","1" => "Yes", "0" => "No"),@$this->params["filter_cancel"]);    //MESSAGE
    $confirm_box   = helper::cms_select_box("filter_confirm","inputbox",array("default" => "select confirmed order","1" => "Yes", "0" => "No"),@$this->params["filter_confirm"]);    //MESSAGE
    // echo "<pre>";
    // print_r($_SESSION);
    // echo "</pre>";
    @$message = Session::get("message");
    $message_cms = helper::cms_message($message);
    Session::delete("message");
?>
        <div id="system-message-container">
            <?php echo $message_cms; ?>
        </div>
		<div id="element-box">
			<div class="m">
				<form action="#" method="post" name="adminForm" id="adminForm">
                	<!-- FILTER -->
                    <fieldset id="filter-bar">
                        <div class="filter-search fltlft">
                            <label class="filter-search-lbl" for="filter_search">Filter:</label>
                            <input type="text" name="filter_search" id="filter_search" value="<?php echo @$this->params["filter_search"]; ?>" title="Search in module title.">
                            <button type="submit" name = "filter_submit">Search</button>
                            <button type="button" name = "filter_clear" >Clear</button>
                        </div>
                        <div class="filter-select fltrt">
                            <?php echo $status_box . $cancel_box . $confirm_box; ?>
                        </div>
                    </fieldset>
					<div class="clr"> </div>

                    <table class="adminlist" id="modules-mgr">
                    	<!-- HEADER TABLE -->
                        <thead>
                            <tr>
                                <th width="1%">
                                    <input type="checkbox" name="checkall-toggle" value="">
                                </th>
                                <th width="5%" class="nowrap"><?php echo $lb_id; ?></th>
                                <th class="10%"><?php echo $lb_name; ?></th>
                                <th width="10%"><a href="#">ID BOOK</a></th>
                                <th width="5%"><a href="#">Status</a></th>
                                <th width="20%"><a href="#">Prices</a></th>
                                <th width="5%"><a href="#">Quantities</a></th>
                                <th width="10%"><a href="#">Books</a></th>
                                <th width="5%"><?php echo $lb_order_name; ?></th>
                                <th width="5%"><?php echo $lb_phone; ?></th>
                                <th width="5%"><?php echo $lb_address; ?></th>
                                <th width="5%"><a href="#">Total price</a></th>
                                <th width="10%" ><?php echo $lb_created; ?></th>
                                <th width="5%"><a href="#">Canceled</a></th>
                                <th width="5%"><a href="#">Confirmed</a></th>
                                
                            </tr>
                        </thead>
                        <!-- FOOTER TABLE -->
                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <!-- PAGINATION -->
                                    <div class="container">
                                        <?php echo $pagination_html; ?>
                                    </div>				
                                </td>
                            </tr>
                        </tfoot>
                        <!-- BODY TABLE -->
						<tbody>
                        <?php 
                            $i = 0;
                            if(!empty($this->items))
                            {
                                $xhtml = "";
                                foreach($this->items as $value)
                                {
                                    $id = $value["id"];
                                    $row = ($i % 2 == 0)?"row0" : "row1";
                                    $status = helper::cms_status($value["status"],"#",$id);
                                    $canceled = helper::cms_canceled($value["canceled"],"#",$id);
                                    $confirm = helper::cms_confirm($value["confirmed"],url::create_url("admin","cart","ajax_confirm",
                                                                    array("id" => $id, "confirm" => $value["confirmed"])),$id);
                                    $books = json_decode($value["books"]);
                                    $prices = json_decode($value["prices"]);
                                    $quantities = json_decode($value["quantities"]);
                                    $names = json_decode($value["names"]);
                                    $str_books = implode("  | ",$books);
                                    $str_prices = "";
                                    $str_quantities = "";
                                    $total_price = 0;
                                    foreach($quantities as $keyb => $valueb)
                                    {
                                        $total_price  += ($valueb*$prices[$keyb]);
                                        if($keyb == 0)
                                        {
                                            $str_prices .= number_format($prices[$keyb]) ;
                                            $str_quantities .= $valueb ;
                                        }
                                        else
                                        {
                                            $str_prices .=  " | " .number_format($prices[$keyb]) ;
                                            $str_quantities .=  " | " .$valueb ;
                                        }
                                        

                                    }
                                    $str_names = implode("&nbsp;&nbsp;|&nbsp;&nbsp ",$names);

                        ?>  
                                <tr class="<?php echo $row; ?>">
                                    <td class="center">
                                        <input type="checkbox" name="cid[]" value="<?php echo $id; ?>" >
                                    </td>
                                    <td class="center"><a href="#"><?php echo $id; ?></a></td>
                                    <td class="center"><?php echo $value["user_id"]; ?></td>
                                    <td class="center"><?php echo $str_books; ?></td>
                                    <td class="center"><?php echo $status; ?></td>
                                    <td class="center"><?php echo $str_prices;?></td>
                                    <td class="center"><?php echo $str_quantities;?></td>
                                    <td class="center"><?php echo $str_names;?></td>
                                    <td class="center"><?php echo $value["order_person"];?></td>
                                    <td class="center"><?php echo $value["phone"];?></td>
                                    <td class="center"><?php echo $value["address"];?></td>
                                    <td class="center"><?php echo number_format($total_price); ?></td>
                                    <td class="center"><?php echo date("H:i:s d/m/Y",strtotime($value["date"])) ; ?></td>
                                    <td class="center"><?php echo $canceled; ?></td>
                                    <td class="center"><?php echo $confirm; ?></td>
                                </tr>	
                        <?php 
                                 $i++;
                                    
                                }
                            }
                        ?>
						</tbody>
					</table>

                    <div>
                        <input type="hidden" name="filter_column" value="">
                        <input type="hidden" name="filter_column_dir" value="">
                        <input type="hidden" name="filter_page" value="1">
                    </div>
                </form>

				<div class="clr"></div>
			</div>
		</div>

       