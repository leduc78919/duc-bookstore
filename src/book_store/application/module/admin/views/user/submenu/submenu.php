<?php 
    $link_group = url::create_url("admin","group","index");
    $link_user = url::create_url("admin","user","index");
	$link_category = url::create_url("admin","category","index");
	$link_book = url::create_url("admin","book","index");
	$link_cart = url::create_url("admin","cart","index");
?>
<div id="submenu-box">
			<div class="m">
				<ul id="submenu">
					<li><a  href="<?php echo $link_group; ?>">GROUP</a></li>
					<li><a  class="active" href="<?php echo $link_user; ?>">USER</a></li>
					<li><a  href="<?php echo $link_category; ?>">CATEGORY</a></li>
					<li><a   href="<?php echo $link_book; ?>">BOOK</a></li>
					<li><a   href="<?php echo $link_cart; ?>">CART</a></li>
				</ul>
				<div class="clr"></div>
			</div>
		</div>