<?php require_once(MODULE_PATH."admin/views/toolbar.php"); ?>
<?php require_once("submenu/submenu.php"); ?>
<?php 
    $column_post = "";
    $column_post_dir = "";
    if(isset($this->params["filter_column"]) && isset($this->params["filter_column_dir"]))
    {
        $column_post = $this->params["filter_column"];
        $column_post_dir = $this->params["filter_column_dir"];
    }
    $lb_user_name   = helper::cms_link_sort("User Name","user_name",$column_post,$column_post_dir);
    $lb_full_name   = helper::cms_link_sort("Full Name","full_name",$column_post,$column_post_dir);
    $lb_group       = helper::cms_link_sort("Group","group_name",$column_post,$column_post_dir);
    $lb_email       = helper::cms_link_sort("Email","email",$column_post,$column_post_dir);
    $lb_id          = helper::cms_link_sort("ID","id",$column_post,$column_post_dir);
    $lb_ordering    = helper::cms_link_sort("Ordering","ordering",$column_post,$column_post_dir);
    $lb_created     = helper::cms_link_sort("Created","created",$column_post,$column_post_dir);
    $lb_created_by  = helper::cms_link_sort("Created_by","created_by",$column_post,$column_post_dir);
    $lb_modified    = helper::cms_link_sort("Modified","modified_by",$column_post,$column_post_dir);
    $lb_modified_by = helper::cms_link_sort("Modified By","modified_by",$column_post,$column_post_dir);

   
    $pagination_html =  $this->pagination->create_html(url::create_url("admin","user","index"));
    //SELECT BOX
    $status_box    = helper::cms_select_box("filter_state","inputbox",array("default" => " -- select_status -- ","1" => "publish", "0" => "unpublish"),@$this->params["filter_state"]);
    $this->group_box["default"] = " -- select group -- ";
    ksort($this->group_box);
    $group_box    = helper::cms_select_box("filter_group_id","inputbox",$this->group_box,@$this->params["filter_group_id"]);

    //MESSAGE
    // echo "<pre>";
    // print_r($_SESSION);
    // echo "</pre>";
    // echo "<pre>";
    // print_r($this->items);
    // echo "</pre>";
    @$message = Session::get("message");
    $message_cms = helper::cms_message($message);
    Session::delete("message");
?>
        <div id="system-message-container">
            <?php echo $message_cms; ?>
        </div>
		<div id="element-box">
			<div class="m">
				<form action="#" method="post" name="adminForm" id="adminForm">
                	<!-- FILTER -->
                    <fieldset id="filter-bar">
                        <div class="filter-search fltlft">
                            <label class="filter-search-lbl" for="filter_search">Filter:</label>
                            <input type="text" name="filter_search" id="filter_search" value="<?php echo @$this->params["filter_search"]; ?>" title="Search in module title.">
                            <button type="submit" name = "filter_submit">Search</button>
                            <button type="button" name = "filter_clear" >Clear</button>
                        </div>
                        <div class="filter-select fltrt">
                            <?php echo $status_box . $group_box;?>
                        </div>
                    </fieldset>
					<div class="clr"> </div>

                    <table class="adminlist" id="modules-mgr">
                    	<!-- HEADER TABLE -->
                        <thead>
                            <tr>
                                <th width="1%">
                                    <input type="checkbox" name="checkall-toggle" value="">
                                </th>
                                <th width="1%" class="nowrap"><?php echo $lb_id; ?></th>
                                <th class="title"><?php echo $lb_user_name; ?></th>
                                <th width="10%"><a href="#"><?php echo $lb_full_name ?></a></th>
                                <th width="10%"><a href="#"><?php echo $lb_email ?></a></th>
                                <th width="10%"><a href="#"><?php echo $lb_group ?></a></th>
                                <th width="10%"><a href="#">Status</a></th>
                                <th width="10%"><?php echo $lb_ordering; ?></th>
                                <th width="5%">Phone</th>
                                <th width="10%">Address</th>
                                <th width="10%" ><?php echo $lb_created; ?></th>
                                <th width="10%"><?php echo $lb_created_by; ?></th>
                                <th width="10%"><?php echo $lb_modified; ?></th>
                                <th width="10%"><?php echo $lb_modified_by; ?></th>
                                
                            </tr>
                        </thead>
                        <!-- FOOTER TABLE -->
                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <!-- PAGINATION -->
                                    <div class="container">
                                        <?php echo $pagination_html; ?>
                                    </div>				
                                </td>
                            </tr>
                        </tfoot>
                        <!-- BODY TABLE -->
						<tbody>
                        <?php 
                            $i = 0;
                            if(!empty($this->items))
                            {
                                $xhtml = "";
                                foreach($this->items as $value)
                                {
                                    $id = $value["id"];
                                    $row = ($i % 2 == 0)?"row0" : "row1";
                                    $status = helper::cms_status($value["status"],url::create_url("admin","user","ajax_status",array("id" => $id, "status" => $value["status"])),$id);
                                    $link_edit = url::create_url("admin","user","form",array("id"=>$id));
                        ?>  
                                <tr class="<?php echo $row; ?>">
                                    <td class="center">
                                        <input type="checkbox" name="cid[]" value="<?php echo $id; ?>" >
                                    </td>
                                    <td class="center"><a href="#"><?php echo $id ?></a></td>
                                    <td class="center"><a href="<?php echo $link_edit; ?>"><?php echo $value["user_name"] ?></a></td>
                                    <td class="center"><?php echo $value["full_name"]?></td>
                                    <td class="center"><?php echo $value["email"]?></td>
                                    <td class="center"><?php echo $value["group_name"]?></td>
                                    <td class="center"><?php echo $status; ?></td>
                                    <td class="order">
                                        <input type="text" name="order[<?php echo $id;?>]" size="5" value="<?php echo $value["ordering"] ?>"  class="text-area-order">
                                    </td>
                                    <td class="center"><?php echo $value["phone"] ?></td>
                                    <td class="center"><?php echo $value["address"] ?></td>
                                    <td class="center"><?php echo helper::format_date("d-m-Y",$value["created"]);?></td>
                                    <td class="center"><?php echo $value["created_by"] ?></td>
                                    <td class="center"><?php echo $value["modified"] ?></td>
                                    <td class="center"><?php echo $value["modified_by"] ?></td>
                                </tr>	
                        <?php 
                                $i++;
                                }
                            }
                        ?>
						</tbody>
					</table>

                    <div>
                        <input type="hidden" name="filter_column" value="">
                        <input type="hidden" name="filter_column_dir" value="">
                        <input type="hidden" name="filter_page" value="1">
                    </div>
                </form>

				<div class="clr"></div>
			</div>
		</div>

       