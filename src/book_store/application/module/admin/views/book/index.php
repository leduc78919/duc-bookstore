<?php require_once(MODULE_PATH."admin/views/toolbar.php"); ?>
<?php require_once("submenu/submenu.php"); ?>
<?php 
    $column_post = "";
    $column_post_dir = "";
    if(isset($this->params["filter_column"]) && isset($this->params["filter_column_dir"]))
    {
        $column_post = $this->params["filter_column"];
        $column_post_dir = $this->params["filter_column_dir"];
    }
    $lb_name        = helper::cms_link_sort("Book Name","name",$column_post,$column_post_dir);
    $lb_price       = helper::cms_link_sort("Price","price",$column_post,$column_post_dir);
    $lb_sale_off    = helper::cms_link_sort("Sale Off","sale_off",$column_post,$column_post_dir);
    $lb_id          = helper::cms_link_sort("ID","id",$column_post,$column_post_dir);
    $lb_ordering    = helper::cms_link_sort("Ordering","ordering",$column_post,$column_post_dir);
    $lb_created     = helper::cms_link_sort("Created","created",$column_post,$column_post_dir);
    $lb_created_by  = helper::cms_link_sort("Created_by","created_by",$column_post,$column_post_dir);
    $lb_modified    = helper::cms_link_sort("Modified","modified_by",$column_post,$column_post_dir);
    $lb_modified_by = helper::cms_link_sort("Modified By","modified_by",$column_post,$column_post_dir);

   
    $pagination_html =  $this->pagination->create_html(url::create_url("admin","book","index"));
    //SELECT BOX
    $status_box    = helper::cms_select_box("filter_state","inputbox",array("default" => " -- select status -- ","1" => "publish", "0" => "unpublish"),@$this->params["filter_state"]);    //MESSAGE
    $special_box   = helper::cms_select_box("filter_special","inputbox",array("default" => " -- select special --","1" => "YES", "0" => "NO"),@$this->params["filter_special"]);    //MESSAGE
    $this->category_box["default"] = " -- select category -- ";
    ksort($this->category_box);
    $category_box    = helper::cms_select_box("filter_group_id","inputbox",$this->category_box,@$this->params["filter_group_id"]);
    // echo "<pre>";
    // print_r($_SESSION);
    // echo "</pre>";
    @$message = Session::get("message");
    $message_cms = helper::cms_message($message);
    Session::delete("message");
?>
        <div id="system-message-container">
            <?php echo $message_cms; ?>
        </div>
		<div id="element-box">
			<div class="m">
				<form action="#" method="post" name="adminForm" id="adminForm">
                	<!-- FILTER -->
                    <fieldset id="filter-bar">
                        <div class="filter-search fltlft">
                            <label class="filter-search-lbl" for="filter_search">Filter:</label>
                            <input type="text" name="filter_search" id="filter_search" value="<?php echo @$this->params["filter_search"]; ?>" title="Search in module title.">
                            <button type="submit" name = "filter_submit">Search</button>
                            <button type="button" name = "filter_clear" >Clear</button>
                        </div>
                        <div class="filter-select fltrt">
                            <?php echo $status_box . $special_box. $category_box; ?>
                        </div>
                    </fieldset>
					<div class="clr"> </div>

                    <table class="adminlist" id="modules-mgr">
                    	<!-- HEADER TABLE -->
                        <thead>
                            <tr>
                                <th width="1%">
                                    <input type="checkbox" name="checkall-toggle" value="">
                                </th>
                                <th width="3%" class="nowrap"><?php echo $lb_id; ?></th>
                                <th class="title"><?php echo $lb_name; ?></th>
                                <th width="15%"><a href="#">Image</a></th>
                                <th width="5%"><a href="#">Status</a></th>
                                <th width="5%"><a href="#">Special</a></th>
                                <th width="20%"><a href="#">Description</a></th>
                                <th width="5%"><?php echo $lb_ordering; ?></th>
                                <th width="10%"><?php echo $lb_price; ?></th>
                                <th width="5%"><?php echo $lb_sale_off; ?></th>
                                <th width="10%" ><?php echo $lb_created; ?></th>
                                <th width="5%"><?php echo $lb_created_by; ?></th>
                                <th width="10%"><?php echo $lb_modified; ?></th>
                                <th width="5%"><?php echo $lb_modified_by; ?></th>
                                <th width="5%"><a href="#">Category</a></th>
                                
                            </tr>
                        </thead>
                        <!-- FOOTER TABLE -->
                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <!-- PAGINATION -->
                                    <div class="container">
                                        <?php echo $pagination_html; ?>
                                    </div>				
                                </td>
                            </tr>
                        </tfoot>
                        <!-- BODY TABLE -->
						<tbody>
                        <?php 
                            $i = 0;
                            if(!empty($this->items))
                            {
                                $xhtml = "";
                                foreach($this->items as $value)
                                {
                                    $id = $value["id"];
                                    $row = ($i % 2 == 0)?"row0" : "row1";
                                    $status   = helper::cms_status($value["status"],url::create_url("admin","book","ajax_status",array("id" => $id, "status" => $value["status"])),$id);
                                    $special = helper::cms_special($value["special"],url::create_url("admin","book","ajax_special",array("id" => $id, "special" => $value["special"])),$id);
                                    $link_edit = url::create_url("admin","book","form",array("id"=>$id));
                                    $picture = (!empty($value["picture"]))?$value["picture"]:"default_img.jpg";
                                    $image_path = TEMPLATE_FILE_PATH."book/".$picture;
                        ?>  
                                <tr class="<?php echo $row; ?>">
                                    <td class="center">
                                        <input type="checkbox" name="cid[]" value="<?php echo $id; ?>" >
                                    </td>
                                    <td class="center"><a href="#"><?php echo $id ?></a></td>
                                    <td class="center"><a href="<?php echo $link_edit; ?>"><?php echo $value["name"] ?></a></td>
                                    <td class="center"><img src="<?php echo $image_path; ?>" alt=""></td>
                                    <td class="center"><?php echo $status; ?></td>
                                    <td class="center"><?php echo $special; ?></td>
                                    <td class="center"><?php echo  substr($value["description"],0,250) ?></td>
                                    <td class="order">
                                        <input type="text" name="order[<?php echo $id;?>]" size="5" value="<?php echo $value["ordering"] ?>"  class="text-area-order">
                                    </td>
                                    <td class="center"><?php echo number_format($value["price"])  ?></td>
                                    <td class="center"><?php echo $value["sale_off"]; ?></td>
                                    <td class="center"><?php echo helper::format_date("d-m-Y",$value["created"]);?></td>
                                    <td class="center"><?php echo $value["created_by"] ?></td>
                                    <td class="center"><?php echo $value["modified"] ?></td>
                                    <td class="center"><?php echo $value["modified_by"] ?></td>
                                    <td class="center"><?php echo $value["category_name"] ?></td>
                                </tr>	
                        <?php 
                                $i++;
                                }
                            }
                        ?>
						</tbody>
					</table>

                    <div>
                        <input type="hidden" name="filter_column" value="">
                        <input type="hidden" name="filter_column_dir" value="">
                        <input type="hidden" name="filter_page" value="1">
                    </div>
                </form>

				<div class="clr"></div>
			</div>
		</div>

       