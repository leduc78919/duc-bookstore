<?php require_once(MODULE_PATH."admin/views/toolbar.php"); ?>
<?php require_once("submenu/submenu.php"); ?>
<?php 
	$xhtml = "";
    if(!empty($this->errors))
    {
       
        foreach($this->errors as $key => $value)
        {
            $xhtml .= helper::cms_message(array("class" => "error", "content" => "$key: $value"));
        }
    }
    $status_box	 = helper::cms_select_box("form[status]","",array("default" => "-- select status --","1" => "publish", "0" => "unpublish"),@$this->result["status"],"width: 140px ;");
    $special_box = helper::cms_select_box("form[special]","",array("default" => "-- select special --","1" => "yes", "0" => "no"),@$this->result["special"],"width: 140px ;");
	$this->category_box["default"] = " -- select category -- ";
    ksort($this->category_box);
    $category_box    = helper::cms_select_box("form[category_id]","inputbox",$this->category_box,@$this->result["category_id"]);
	@$message = Session::get("message");
    $message_cms = helper::cms_message($message);
    Session::delete("message");
?>
 <div id="system-message-container">
            <?php echo $message_cms . $xhtml; ?>
</div>
<div id="element-box">
			<div class="m">
				<form action="#" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
					<!-- FORM LEFT -->
					<div class="width-100 fltlft">
						<fieldset class="adminform">
							<legend>Details</legend>
							<ul class="adminformlist" >
								<li>
									<label>Name<span class="star">&nbsp;*</span></label>
									<input type="text" name="form[name]" id="name" value="<?php echo @$this->result["name"]; ?>" class="inputbox required" size="40">
                                    
                                </li>
								<li>
									<label>Status</label>
									<?php echo $status_box; ?>
								</li>
								<li>
									<label>Special</label>
									<?php echo $special_box; ?>
								</li>
								<li>
									<label>Category</label>
									<?php echo $category_box; ?>
								</li>
								<li>
									<label>Ordering</label>
									<input type="text" name="form[ordering]" id="ordering" value="<?php echo @$this->result["ordering"]; ?>" class="inputbox" size="40">
								</li>
								<li>
									<label>Price</label>
									<input type="text" name="form[price]" id="price" value="<?php echo @$this->result["price"]; ?>" class="inputbox" size="40">
								</li>
								<li>
									<label>Sale off</label>
									<input type="text" name="form[sale_off]" id="sale_off" value="<?php echo @$this->result["sale_off"]; ?>" class="inputbox" size="40">
								</li>
								<li>
									<label>Description</label>
									<textarea name="form[description]" cols="30" rows="10"><?php echo @$this->result["description"]; ?></textarea>
								</li>
								<?php if(isset($this->params["id"]) || isset($this->params["box_id"])){ ?>
								<li>
										<label>ID</label>
										<?php $id = isset($this->params["id"])?$this->params["id"]:$this->params["box_id"] ?>
										<input type="text" name="box_id" id="id" value="<?php echo $id?>" class="inputbox" readonly size="40">
								</li>
								<?php } ?>
								<li>
										<label>FILE</label>
										<input type="file" name="file" id="file" value="<?php ?>" class="inputbox" size="40">
								</li>
								<li>
									<label for=""> </label>
									<?php if(!empty($this->result["picture"])){ ?>
									<img src="<?php echo "./public/files/book/". $this->result["picture"]; ?>" alt="">
									<?php }?>
								</li>
							</ul>
							<div class="clr"></div>
							<div>
								<input type="hidden" name="form[token]" value="<?php echo time(); ?>">
								<input type="hidden" name="form[created]" value="<?php echo date("Y-m-d",time()); ?>">
								<input type="hidden" name="form[modified]" value="<?php echo date("Y-m-d",time()); ?>">
								<input type="hidden" name="form[file_hidden]" value="<?php echo @$this->result["picture"]; ?>">
							</div>
						</fieldset>
					</div>
					<div class="clr"></div>
					<div>
					</div>
				</form>
				<div class="clr"></div>
			</div>
		</div>