<?php require_once(MODULE_PATH."admin/views/toolbar.php"); ?>
<?php require_once("submenu/submenu.php"); ?>
<?php  
	$xhtml = "";
    if(!empty($this->errors))
    {
       
        foreach($this->errors as $key => $value)
        {
            $xhtml .= helper::cms_message(array("class" => "error", "content" => "$key: $value"));
        }
    }
    $status_box = helper::cms_select_box("form[status]","",array("default" => "-- select status --","1" => "publish", "0" => "unpublish"),@$this->result["status"],"width: 140px ;");
    $group_acp_box = helper::cms_select_box("form[group_acp]","",array("default" => "-- select group acp -- ","1" => "Yes", "0" => "No"),@$this->result["group_acp"],"width: 140px ;");

	@$message = Session::get("message");
    $message_cms = helper::cms_message($message);
    Session::delete("message");
?>
 <div id="system-message-container">
            <?php echo $message_cms . $xhtml; ?>
</div>
<div id="element-box">
			<div class="m">
				<form action="#" method="post" name="adminForm" id="adminForm" class="form-validate">
					<!-- FORM LEFT -->
					<div class="width-100 fltlft">
						<fieldset class="adminform">
							<legend>Details</legend>
							<ul class="adminformlist" >
								<li>
									<label>Name<span class="star">&nbsp;*</span></label>
									<input type="text" name="form[name]" id="name" value="<?php echo @$this->result["name"]; ?>" class="inputbox required" size="40">
                                    
                                </li>
								<li>
									<label>Status</label>
									<?php echo $status_box; ?>
								</li>
								<li>
									<label>Group ACP</label>
									<?php echo $group_acp_box; ?>
								</li>
								<li>
										<label>Ordering</label>
										<input type="text" name="form[ordering]" id="ordering" value="<?php echo @$this->result["ordering"]; ?>" class="inputbox" size="40">
								</li>
								<?php if(isset($this->params["id"]) || isset($this->params["box_id"])){ ?>
								<li>
										<label>ID</label>
										<?php $id = isset($this->params["id"])?$this->params["id"]:$this->params["box_id"] ?>
										<input type="text" name="box_id" id="id" value="<?php echo $id?>" class="inputbox" readonly size="40">
								</li>
								<?php } ?>
							</ul>
							<div class="clr"></div>
							<div>
								<input type="hidden" name="form[token]" value="<?php echo time(); ?>">
								<input type="hidden" name="form[created]" value="<?php echo date("Y-m-d",time()); ?>">
								<input type="hidden" name="form[modified]" value="<?php echo date("Y-m-d",time()); ?>">
							</div>
						</fieldset>
					</div>
					<div class="clr"></div>
					<div>
					</div>
				</form>
				<div class="clr"></div>
			</div>
		</div>