<?php
$controller = $this->params["controller"];
$arr_button_index = array(
    //new
    "new"       => array(
        "module" => "admin", "controller" => "$controller", "action" => "form",
        "name" => "New", "id" => "toolbar-popup-new", "icon" => "icon-32-new", "type" => "new"
    ),
    //publish
    "publish"   => array(
        "module" => "admin", "controller" => "$controller", "action" => "status", "params" => array("type" => 1),
        "name" => "Publish", "id" => "toolbar-publish", "icon" => "icon-32-publish", "type" => "submit"
    ),
    //unpublish
    "unpublish" => array(
        "module" => "admin", "controller" => "$controller", "action" => "status", "params" => array("type" => 0),
        "name" => "Unpublish", "id" => "toolbar-unpublish", "icon" => "icon-32-unpublish", "type" => "submit"
    ),
    //ordering
    "ordering" => array(
        "module" => "admin", "controller" => "$controller", "action" => "ordering",
        "name" => "ordering", "id" => "toolbar-checkin", "icon" => "icon-32-checkin", "type" => "submit"
    ),
    //trash
    "trash"     => array(
        "module" => "admin", "controller" => "$controller", "action" => "trash",
        "name" => "Trash", "id" => "toolbar-trash", "icon" => "icon-32-trash", "type" => "submit"
    ),
);

$arr_button_form = array(
    //save
    "save"       => array(
        "module" => "admin", "controller" => "$controller", "action" => "form", "params" => array("type" => "save"),
        "name" => "Save", "id" => "toolbar-apply", "icon" => "icon-32-apply", "type" => "submit"
    ),
    //save & close
    "save&close"   => array(
        "module" => "admin", "controller" => "$controller", "action" => "form", "params" => array("type" => "save_close"),
        "name" => "Save & Close", "id" => "toolbar-save", "icon" => "icon-32-save", "type" => "submit"
    ),
    //save & new
    "save&new" => array(
        "module" => "admin", "controller" => "$controller", "action" => "form", "params" => array("type" => "save_new"),
        "name" => "Save & New", "id" => "toolbar-save-new", "icon" => "icon-32-save-new", "type" => "submit"
    ),
    //cancel
    "cancel"     => array(
        "module" => "admin", "controller" => "$controller", "action" => "index",
        "name" => "Cancel", "id" => "toolbar-cancel", "icon" => "icon-32-cancel", "type" => "new"
    ),
);

$arr_button_profile = array(
    //save
    "save"       => array(
        "module" => "admin", "controller" => "$controller", "action" => "profile", "params" => array("type" => "save"),
        "name" => "Save", "id" => "toolbar-apply", "icon" => "icon-32-apply", "type" => "submit"
    ),
    //save & close
    "save&close"   => array(
        "module" => "admin", "controller" => "$controller", "action" => "profile", "params" => array("type" => "save_close"),
        "name" => "Save & Close", "id" => "toolbar-save", "icon" => "icon-32-save", "type" => "submit"
    ),
    //cancel
    "cancel"     => array(
        "module" => "admin", "controller" => "$controller", "action" => "index",
        "name" => "Cancel", "id" => "toolbar-cancel", "icon" => "icon-32-cancel", "type" => "new"
    ),
);
switch ($this->params["action"]) {
    case "index":
        $buttons = helper::cms_button($arr_button_index);
        break;
    case "form":
        $buttons = helper::cms_button($arr_button_form);
        break;
    case "profile":
        $buttons = helper::cms_button($arr_button_profile);
        break;
}

?>
<div id="toolbar-box">
    <div class="m">
        <?php if($this->params["controller"] != "cart" ) {?>
        <!-- TOOLBAR -->
        <div class="toolbar-list" id="toolbar">
            <ul>
                <?php echo $buttons; ?>
                <li class="divider"></li>
            </ul>
            <div class="clr"></div>
        </div>
        <?php } ?>
        <!-- TITLE -->
        <div class="pagetitle icon-48-groups">
            <h2><?php echo $this->title; ?></h2>
        </div>
    </div>
</div>