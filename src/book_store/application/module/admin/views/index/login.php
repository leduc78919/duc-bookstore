<div id="element-box" class="login">
    <div class="m wbg">
        <h1>Administration Login</h1>
        <!-- ERROR -->
        <div id="system-message-container">
            <?php 
                if(isset($this->error_message))
                {
                    echo '<dl id="system-message">
                            <dt class="error">Error</dt>
                            <dd class="error message">
                                <ul>
                                    <li>'.$this->error_message .'</li>
                                </ul>
                            </dd>
                        </dl>';
                }
            ?>
            
        </div>

        <div id="section-box">
            <div class="m">
                <form action="#" method="post" id="form-login">
                    <fieldset class="loginform">
                        <label>User Name</label>
                        <input name="form[user_name]" id="mod-login-username" type="text" class="inputbox" size="15" />
                        <label id="mod-login-password-lbl" for="mod-login-password">Password</label>
                        <input name="form[password]" id="mod-login-password" type="password" class="inputbox" size="15" />
                        <input type="hidden" name="form[token]" value="<?php echo time();?>">
                        <div class="button-holder">
                            <div class="button1">
                                <div class="next">
                                    <a href="#" onclick="document.getElementById('form-login').submit();">Log in</a>
                                </div>
                            </div>
                        </div>
                        <div class="clr"></div>
                    </fieldset>
                </form>
                <div class="clr"></div>
            </div>
        </div>

        <p>Use a valid username and password to gain access to the administrator backend.</p>
        <p><a href="http://localhost/joomla/">Go to site home page.</a></p>
        <div id="lock"></div>
    </div>
</div>