<?php $link_img = TEMPLATE_LINK_PATH."admin/main/".$this->imgs ."/";?>
<div id="element-box">
    <div id="system-message-container"></div>
    <div class="m">
        <div class="adminform">
            <div class="cpanel-left">
                <div class="cpanel">
                    <div class="icon-wrapper">
                        <div class="icon">
                            <a href="<?php echo $link_add_book; ?>">
                                <img src="<?php echo $link_img?>header/icon-48-article-add.png" alt="">
                                <span>Add New Book</span>
                            </a>
                        </div>
                    </div>
                    <div class="icon-wrapper">
                        <div class="icon">
                            <a href="<?php echo $link_book; ?>">
                                <img src="<?php echo $link_img?>header/icon-48-article.png" alt="">
                                <span>Book Manager</span>
                            </a>
                        </div>
                    </div>
                    <div class="icon-wrapper">
                        <div class="icon">
                            <a href="<?php echo @$link_categories ?>">
                                <img src="<?php echo $link_img?>header/icon-48-category.png" alt="">
                                <span>Category Manager</span>
                            </a>
                        </div>
                    </div>
                    <div class="icon-wrapper">
                        <div class="icon">
                            <a href="<?php echo @$link_group_manager ?>">
                                <img src="<?php echo $link_img?>header/icon-48-groups.png" alt="">
                                <span>Group Manager</span>
                            </a>
                        </div>
                    </div>
                    <div class="icon-wrapper">
                        <div class="icon">
                            <a href="<?php echo @$link_user_manager ?>">
                                <img src="<?php echo $link_img?>header/icon-48-user.png" alt="">
                                <span>User Manager</span>
                            </a>
                        </div>
                    </div>
                    <div class="icon-wrapper">
                        <div class="icon">
                            <a href="<?php echo @$link_cart_manager ?>">
                                <img src="<?php echo $link_img?>header/icon-48-generic.png" alt="">
                                <span>Cart Manager</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="clr"></div>
    </div>
</div>