<?php require_once(MODULE_PATH."admin/views/toolbar.php"); ?>
<?php  
	$xhtml = "";
    if(!empty($this->errors))
    {
       
        foreach($this->errors as $key => $value)
        {
			$key = ucwords(str_replace("_"," ",$key)) ;
            $xhtml .= helper::cms_message(array("class" => "error", "content" => "$key: $value"));
        }
    }
    $status_box = helper::cms_select_box("form[status]","",array("default" => "-- select status --","1" => "publish", "0" => "unpublish"),@$this->result["status"],"width: 140px ;");
    $this->group_box["default"] = " -- select group -- ";
    ksort($this->group_box);
	$group_box    = helper::cms_select_box("form[group_id]","",$this->group_box,@$this->result["group_id"]);

	@$message = Session::get("message");
    $message_cms = helper::cms_message($message);
    Session::delete("message");
?>
 <div id="system-message-container">
            <?php echo $message_cms . $xhtml; ?>
</div>
<div id="element-box">
			<div class="m">
				<form action="#" method="post" name="adminForm" id="adminForm" class="form-validate">
					<!-- FORM LEFT -->
					<div class="width-100 fltlft">
						<fieldset class="adminform">
							<legend>Details</legend>
							<ul class="adminformlist" >
								<li>
									<label>User Name<span class="star">&nbsp;*</span></label>
									<input type="text" name="form[user_name]" id="user_name" value="<?php echo @$this->result["user_name"]; ?>" class="inputbox required" size="40">
                                    
                                </li>
								<li>
									<label>Full Name<span class="star">&nbsp;*</span></label>
									<input type="text" name="form[full_name]" id="full_name" value="<?php echo @$this->result["full_name"]; ?>" class="inputbox required" size="40">
                                    
                                </li>
								<li>
									<label>Email<span class="star">&nbsp;*</span></label>
									<input type="text" name="form[email]" id="email" value="<?php echo @$this->result["email"]; ?>" class="inputbox required" size="40">
                                    
                                </li>
								<li>
									<label>Pass Word<span class="star">&nbsp;*</span></label>
									<input type="text" name="form[password]" id="password" value="<?php 
										if(!isset($this->params["id"]) AND !isset($this->params["box_id"])) {echo @$this->result["password"];} ?>" class="inputbox required" size="40">
                                    
                                </li>
								<li>
									<label>Status</label>
									<?php echo $status_box; ?>
								</li>
								<li>
									<label>Group</label>
									<?php echo $group_box; ?>
								</li>
								<li>
										<label>ID</label>
										<input type="text" name="box_id" id="id" value="<?php echo $this->result["id"]?>" class="inputbox" readonly size="40">
								</li>
							</ul>
							<div class="clr"></div>
							<div>
								<input type="hidden" name="form[token]" value="<?php echo time(); ?>">
								<input type="hidden" name="form[created]" value="<?php echo date("Y-m-d",time()); ?>">
								<input type="hidden" name="form[modified]" value="<?php echo date("Y-m-d",time()); ?>">
							</div>
						</fieldset>
					</div>
					<div class="clr"></div>
					<div>
					</div>
				</form>
				<div class="clr"></div>
			</div>
		</div>