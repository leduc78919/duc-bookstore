<?php 
class  cart_controller extends controller
{
    private $_columns = array("id","name","status","picture","ordering","created","created_by","modified","modified_by");
    public function __construct()
    {
       parent::__construct();
    }


    // hien thi danh sach group
    public function index_action()
    {
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("Cart Manager ");
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 5,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items = $this->db->get_items($this->params,null);
        $this->view->render("cart/index");
    }
    
    public function ajax_confirm_action()
    {
        $result = $this->db->update_status($this->params, $options = array("task" => "change_ajax_confirm"));
        echo json_encode($result);
    }

   
}