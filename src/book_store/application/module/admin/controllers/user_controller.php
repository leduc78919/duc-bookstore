<?php 
class user_controller extends controller
{
    private $_columns = array("id","user_name","email","full_name","password","created","created_by",
                            "modified","modified_by","status","ordering","register_date","register_ip",
                            "visited_date","visited_ip","group_id");
    public function __construct()
    {
       parent::__construct();
    }


    // hien thi danh sach group
    public function index_action()
    {
        // echo "<pre>";
        // print_r($this->params);
        // echo "</pre>";
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("User Manager: User");
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 5,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items = $this->db->get_items($this->params,null);
        $this->view->group_box = $this->db->item_in_box($this->params);
        $this->view->render("user/index");
    }

    // them va chinh sua danh sach
    public function form_action()
    {
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("User Manager: User : Add");
        $this->view->group_box = $this->db->item_in_box($this->params);
        if(isset($this->params["id"]))
        {
            $this->view->set_title("User Manager: User: EDIT");
            $this->view->result = $this->db->single_item($this->params);
        }
        if(isset($this->params["form"]["token"]))
        {
            $session = $_SESSION["user"]["info"];
            $validate = new validate($this->params["form"]);
            $query_user = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE user_name = "'.trim( $this->params["form"]["user_name"]).' "';
            $query_email = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE email     = "'.trim( $this->params["form"]["email"]).' "';
            if(isset($this->params["box_id"]))
            {
                $query_user .= 'AND id <> '.$this->params["box_id"].'';
                $query_email .=  'AND id <> '.$this->params["box_id"].'';
                
            }
            $validate->addRule("user_name","string-exit", array("min" => 3, "max" => 255, "id" => "user_name","database"=>$this->db,"query" => $query_user))
                        ->addRule("full_name","string", array("min" => 3, "max" => 255, "id" => "full_name"))
                        ->addRule("email","email-exit",array("database"=>$this->db,"query" => $query_email))
                        ->addRule("status","status",array("deny" => array("default"),"id" => "status"))
                        ->addRule("ordering","numeric",array("min"=>1,"max"=>100,"id" => "ordering"))
                        ->addRule("group_id","status",array("deny" => array("default"),"id" => "group_id"));
            if($this->params["form"]["password"] != null)
            {
                $validate->addRule("password","password",array("id" => "password"));
            }
            else
            {
                unset($this->params["form"]["password"]);
            }
                    
            $validate->run();
            $this->view->result = $validate->showResult();
            if(!$validate->isValid())
            {
                $this->view->errors = $validate->showError();
            }
            else
            { 
                if($this->params["form"]["phone"] != null)
                {
                    $this->_columns[] = "phone";
                }
                if($this->params["form"]["address"] != null)
                {
                    $this->_columns[] = "address";
                }

                $result_merge = array_intersect_key($this->params["form"],array_flip($this->_columns));
                if(isset($this->params["box_id"]))
                {
                    $result_merge["modified"] = date("Y-m-d H:m:s",time());
                    $result_merge["modified_by"] = (int)$session["id"];
                    $id_return = $this->db->save_item($result_merge,array("task" => "edit","id" => $this->params["box_id"]));
                }
                else
                {
                    $result_merge["created"] = date("Y-m-d H:m:s",time());
                    $result_merge["created_by"] = (int)$session["id"];
                    $id_return = $this->db->save_item($result_merge,array("task" => "add"));
                }
                $type = $this->params["type"];
                if($type == "save_close")
                {
                    helper::redirect("admin","user","index");
                }
                else if($type == "save_new")
                {
                    helper::redirect("admin","user","form");
                }
                else if($type == "save")
                {
                    helper::redirect("admin","user","form",array("id" => $id_return));
                }
            }
            
            
        }
        $this->view->render("user/form");
    }

    // ajax action
    public function ajax_status_action()
    {
        $result = $this->db->update_status($this->params, $options = array("task" => "change_ajax_status"));
        echo json_encode($result);
    }

    public function status_action()
    {
        $this->db->update_status($this->params, $options = array("task" => "change_all_status"));
        header("location: ". url::create_url("admin","user" ,"index"));
        exit();
    }

    public function ordering_action()
    {
        $this->db->ordering($this->params);
        header("location: ". url::create_url("admin","user" ,"index"));
        exit();
    }

    public function trash_action()
    {
        $this->db->delete_status($this->params);
        header("location: ". url::create_url("admin","user" ,"index"));
        exit();
    }
    // public function login_action()
    // {
    //     $this->template_obj->set_all('admin/main','index.php','template.ini');
    //     $this->view->append_css(array("user/css/style.css"));
    //     $this->view->append_js(array("user/js/test.js"));
    //     $this->view->render("user/login",true);
    // }

    // public function logout_action()
    // {
    //     $this->template_obj->set_all('admin/main','logout.php','template.ini');
    //     $this->view->render("user/logout",true);
    // }
}