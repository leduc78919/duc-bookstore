<?php 
class group_controller extends controller
{
    private $_columns = array("id","name","status","group_acp","ordering","created","created_by","modified","modified_by");
    public function __construct()
    {
       parent::__construct();
    }


    // hien thi danh sach group
    public function index_action()
    {
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("User Manager: User Groups");
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 5,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items = $this->db->get_items($this->params,null);
        $this->view->render("group/index");
    }

    // them va chinh sua danh sach
    public function form_action()
    {
        // echo "<pre>";
        // print_r($_SESSION["user"]["info"]);
        // echo "</pre>";
        $session = $_SESSION["user"]["info"];
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("User Manager: User Groups: Add");
        if(isset($this->params["id"]))
        {
            $this->view->set_title("User Manager: User Groups: EDIT");
            $this->view->result = $this->db->single_item($this->params);
           
           
        }
        if(isset($this->params["form"]["token"]))
        {
           $validate = new validate($this->params["form"]);
           $validate->addRule("name","string", array("min" => 3, "max" => 255, "id" => "name"))
                    ->addRule("status","status",array("deny" => array("default"),"id" => "status"))
                    ->addRule("ordering","numeric",array("min"=>1,"max"=>100,"id" => "ordering"))
                    ->addRule("group_acp","status",array("deny" => array("default"),"id" => "Group Acp"));
            $validate->run();
            $this->view->result = $validate->showResult();
            if(!$validate->isValid())
            {
                $this->view->errors = $validate->showError();
            }
            else
            { 
                $result_merge = array_intersect_key($this->params["form"],array_flip($this->_columns));
                if(isset($this->params["box_id"]))
                {
                    $result_merge["modified"] = date("Y-m-d H:m:s",time());
                    $result_merge["modified_by"] = (int)$session["id"];
                    $id_return = $this->db->save_item($result_merge,array("task" => "edit","id" => $this->params["box_id"]));
                }
                else
                {
                    $result_merge["created"] = date("Y-m-d H:m:s",time());
                    $result_merge["created_by"] = (int)$session["id"];
                    $id_return = $this->db->save_item($result_merge,array("task" => "add"));
                }
                $type = $this->params["type"];
                if($type == "save_close")
                {
                    helper::redirect("admin","group","index");
                }
                else if($type == "save_new")
                {
                    helper::redirect("admin","group","form");
                }
                else if($type == "save")
                {
                    helper::redirect("admin","group","form",array("id" => $id_return));
                }
            }
            
            
        }
        $this->view->render("group/form");
    }

    // ajax action
    public function ajax_status_action()
    {
        $result = $this->db->update_status($this->params, $options = array("task" => "change_ajax_status"));
        echo json_encode($result);
    }

    public function group_acp_status_action()
    {
        $result = $this->db->update_status($this->params, $options = array("task" => "change_group_acp_status"));
        echo json_encode($result);
    }

    public function status_action()
    {
        $this->db->update_status($this->params, $options = array("task" => "change_all_status"));
        header("location: ". url::create_url("admin","group" ,"index"));
        exit();
    }

    public function ordering_action()
    {
        $this->db->ordering($this->params);
        header("location: ". url::create_url("admin","group" ,"index"));
        exit();
    }

    public function trash_action()
    {
        $this->db->delete_status($this->params);
        header("location: ". url::create_url("admin","group" ,"index"));
        exit();
    }
    // public function login_action()
    // {
    //     $this->template_obj->set_all('admin/main','index.php','template.ini');
    //     $this->view->append_css(array("user/css/style.css"));
    //     $this->view->append_js(array("user/js/test.js"));
    //     $this->view->render("user/login",true);
    // }

    // public function logout_action()
    // {
    //     $this->template_obj->set_all('admin/main','logout.php','template.ini');
    //     $this->view->render("user/logout",true);
    // }
}