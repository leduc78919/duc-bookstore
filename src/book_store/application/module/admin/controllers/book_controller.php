<?php 
class   book_controller extends controller
{
    private $_columns = array("id","name","status","picture","ordering","created","created_by","modified","modified_by",
                            "price","sale_off","description","special","category_id");
    public function __construct()
    {
       parent::__construct();
    }


    // hien thi danh sach group
    public function index_action()
    {
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("Book Manager ");
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 5,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items = $this->db->get_items($this->params,null);
        $this->view->category_box = $this->db->item_in_box($this->params);
        $this->view->render("book/index");
    }

    // them va chinh sua danh sach
    public function form_action()
    {
        $this->view->category_box = $this->db->item_in_box($this->params);
        if(!empty($_FILES["file"]))
        {
            $this->params["form"]["file"] = $_FILES["file"];
        }
        $session = $_SESSION["user"]["info"];
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("Book Manager: Add");
        if(isset($this->params["id"]))
        {
            $this->view->set_title("Book Manager: EDIT");
            $this->view->result = $this->db->single_item($this->params);
        }
        if(isset($this->params["form"]["token"]))
        {
           $validate = new validate($this->params["form"]);
           $validate->addRule("name","string", array("min" => 3, "max" => 255, "id" => "name"))
                    ->addRule("status","status",array("deny" => array("default"),"id" => "status"))
                    ->addRule("special","status",array("deny" => array("default"),"id" => "special"))
                    ->addRule("category_id","status",array("deny" => array("default"),"id" => "category"))
                    ->addRule("ordering","numeric",array("min"=>1,"max"=>100000,"id" => "ordering"))
                    ->addRule("price","numeric",array("min"=>1,"max"=>10000000,"id" => "price"))
                    ->addRule("sale_off","number",array("min"=>0,"max"=>100,"id" => "sale_off"))
                    ->addRule("description","string", array("min" => 3, "max" => 86888788, "id" => "description"))
                    ->addRule("file","file",array("min" => MIN_FILE,"max" => MAX_FILE,"extension" => EXTENSION_FILE));
            $validate->run();
            $this->view->result = $validate->showResult();
            if(!$validate->isValid())
            {
                $this->view->errors = $validate->showError();
            }
            else
            { 
                if($this->params["form"]["file"]["tmp_name"] != null)
                {
                    $this->_columns[] = "file";
                }
                $result_merge = array_intersect_key($this->params["form"],array_flip($this->_columns));
                if(isset($this->params["box_id"]))
                {
                    $result_merge["picture"] = $this->params["form"]["file_hidden"];
                    $result_merge["modified"] = date("Y-m-d H:m:s",time());
                    $result_merge["modified_by"] = (int)$session["id"];
                    $id_return = $this->db->save_item($result_merge,array("task" => "edit","id" => $this->params["box_id"]));
                }
                else
                {
                    $result_merge["created"] = date("Y-m-d H:m:s",time());
                    $result_merge["created_by"] = (int)$session["id"];
                    $id_return = $this->db->save_item($result_merge,array("task" => "add"));
                }
                $type = $this->params["type"];
                if($type == "save_close")
                {
                    helper::redirect("admin","book","index");
                }
                else if($type == "save_new")
                {
                    helper::redirect("admin","book","form");
                }
                else if($type == "save")
                {
                    helper::redirect("admin","book","form",array("id" => $id_return));
                }
            }
            
            
        }
        $this->view->render("book/form");
    }

    // ajax action
    public function ajax_status_action()
    {
        $result = $this->db->update_status($this->params, $options = array("task" => "change_ajax_status"));
        echo json_encode($result);
    }

    public function ajax_special_action()
    {
        $result = $this->db->update_status($this->params, $options = array("task" => "change_ajax_special"));
        echo json_encode($result);
    }

    public function status_action()
    {
        $this->db->update_status($this->params, $options = array("task" => "change_all_status"));
        header("location: ". url::create_url("admin","book" ,"index"));
        exit();
    }

    public function ordering_action()
    {
        $this->db->ordering($this->params);
        header("location: ". url::create_url("admin","book" ,"index"));
        exit();
    }

    public function trash_action()
    {
        $this->db->delete_status($this->params,array("folder_file" => "book/"));
        header("location: ". url::create_url("admin","book" ,"index"));
        exit();
    }
    // public function login_action()
    // {
    //     $this->template_obj->set_all('admin/main','index.php','template.ini');
    //     $this->view->append_css(array("user/css/style.css"));
    //     $this->view->append_js(array("user/js/test.js"));
    //     $this->view->render("user/login",true);
    // }

    // public function logout_action()
    // {
    //     $this->template_obj->set_all('admin/main','logout.php','template.ini');
    //     $this->view->render("user/logout",true);
    // }
}