<?php 
class index_controller extends controller
{
    private $_columns = array("id","user_name","email","full_name","password","created","created_by",
                            "modified","modified_by","status","ordering","register_date","register_ip",
                            "visited_date","visited_ip","group_id");
    public function login_action()
    { 

        if($logged = (isset($_SESSION["user"]["login"]) && ($_SESSION["user"]["login"] == true) && ($_SESSION["user"]["time"] + TIME_LOGIN > time())))
        {
            helper::redirect("admin","index","index");
        }
        $this->template_obj->set_all('admin/main','login.php','template.ini');
        if(isset($this->params["form"]["token"]))
        {
            $user_name = $this->params["form"]["user_name"];
            $password  = $this->params["form"]["password"];
            $pattern = array('#<script ([^>]*)>#','#</script>#','#([^A-Za-z0-9@_\s] *)#');
            $replace = array('&lt;script //1&gt;','&lt;/script&gt;','    ');
            $user_name = preg_replace($pattern,$replace,$user_name);
            $password  = preg_replace($pattern,$replace,$password);
            $item = $this->db->check_login(array("user_name" => $user_name,"password" => $password));
            if(empty($item))
            {
                $this->view->error_message = "User Name OR Password is not correct";
            }
            else
            {
                $info_user = $this->db->single_item($this->params["form"]);
                $array_session = array(
                    "login" => true,
                    "info"  => $info_user,
                    "time"  => time(),
                    "group_acp" => $info_user["group_acp"]
                );
                Session::set("user",$array_session);
                helper::redirect("admin","index","index");
            }
        }
       
        $this->view->render("index/login");
    }

    public function logout_action()
    {
        if(isset($_SESSION["cart"]))
        {
            foreach($_SESSION["cart"]["quantity"] as $key => $value)
            {
                $this->params["books"][] = $key;
                $this->params["prices"][] = $_SESSION["cart"]["price"][$key];
                $this->params["quantities"][] = $value;
                $this->params["user_id"] = $_SESSION["user"]["info"]["id"];
            }
            $this->db->save_item($this->params,array("task" => "save_pre_order"));
        }
        Session::delete("user");
        helper::redirect("admin","index","login");
    }

    public function index_action()
    {
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->render("index/index");
    }

    public function profile_action()
    {
        // echo "<pre>";
        // print_r($_SESSION);
        // echo "</pre>";
        // die();
        $session = $_SESSION["user"]["info"];
        $this->template_obj->set_all('admin/main','index.php','template.ini');
        $this->view->set_title("PROFILE");
        $this->view->group_box = $this->db->item_in_box($this->params);
        $this->view->result = $session;
        if(isset($this->params["form"]["token"]))
        {
            $validate = new validate($this->params["form"]);
            $query_user = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE user_name = "'.trim( $this->params["form"]["user_name"]).' "';
            $query_email = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE email     = "'.trim( $this->params["form"]["email"]).' "';
            $query_user .= 'AND id <> '.$this->params["box_id"].'';
            $query_email .=  'AND id <> '.$this->params["box_id"].'';
            $validate->addRule("user_name","string-exit", array("min" => 3, "max" => 255, "id" => "user_name","database"=>$this->db,"query" => $query_user))
                    ->addRule("full_name","string", array("min" => 3, "max" => 255, "id" => "full_name"))
                    ->addRule("email","email-exit",array("database"=>$this->db,"query" => $query_email))
                    ->addRule("status","status",array("deny" => array("default"),"id" => "status"))
                    ->addRule("group_id","status",array("deny" => array("default"),"id" => "group_id"))
                    ->addRule("password","password",array("id" => "password"));  
            $validate->run();
            $this->view->result = $validate->showResult();
            if(!$validate->isValid())
            {
                $this->view->errors = $validate->showError();
            }
            else
            { 
                $result_merge = array_intersect_key($this->params["form"],array_flip($this->_columns));
                if(isset($this->params["box_id"]))
                {
                    $id_return = $this->db->save_item($result_merge,array("task" => "edit","id" => $this->params["box_id"]));
                    $item_info = $this->db->save_session_after_edit($id_return);
                    $_SESSION["user"]["info"] = $item_info;

                }
                $type = $this->params["type"];
                if($type == "save_close")
                {
                    helper::redirect("admin","index","index");
                }
                else if($type == "save")
                {
                    helper::redirect("admin","index","profile");
                }
            }
            
            
        }
        $this->view->render("index/profile");
    }

    
}