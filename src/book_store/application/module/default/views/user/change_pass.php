<div class="title"><span class="title_icon"><img src="<?php echo $link_img?>bullet1.gif" alt="" title="" /></span>Change password</div>

<div class="feat_prod_box_details">
    <div class="contact_form">
        <div class="form_subtitle">Change password</div>
        
        <?php  
            $error_noti = "";
            if(!isset($_SESSION["message_error_pass"]))
            {
                if(!empty($this->errors))
                {
                    foreach($this->errors as $key =>  $value)
                    {
                        $error_noti .= '<div class="error-public">'.$key.' : '.$value.'</div>';
                    }
                }
            }
            else 
            {
                $error_noti .= '<div class="error-public">'.$_SESSION["message_error_pass"].'</div>';
                Session::delete("message_error_pass");
            }
            $message = "";
            if(isset($_SESSION["message_change_pass_success"]))
            {
                $message .= '<div class = "message_save">'.$_SESSION["message_change_pass_success"].'</div>';
                Session::delete("message_change_pass_success");
            }
            echo $error_noti . $message;
        ?>
        <form name="register" action="#" method="POST">
            <div class="form_row">
                <label class=""><strong>Recent password:</strong></label>
                <input type="password" class="contact_input"  name="form[recent_pass]" value="<?php echo @$this->result["recent_pass"]; ?>" />
            </div>
            <div class="form_row">
                <label class=""><strong>New password:</strong></label>
                <input type="password" class="contact_input" name="form[new_password]" value="<?php echo @$this->result["new_password"]; ?>" />
            </div>
            <div class="form_row">
                <label class=""><strong>Rewrite new password:</strong></label>
                <input type="password" class="contact_input" name="form[rewrite_new_password]" value="<?php echo @$this->result["rewrite_new_password"]; ?>" />
            </div>
            <div class="form_row">
                <input type="hidden" name="form[token]" value="<?php echo time(); ?>">
                <input type="submit" class="register" name="form[submit]" value="save" />
            </div>
        </form>
    </div>

</div>
<div class="clear"></div>