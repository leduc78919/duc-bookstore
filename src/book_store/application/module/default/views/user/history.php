<?php 
    $xhtml = "";
    $pagination_html =  "";
    if(!empty($this->items))
    {
        $pagination_html =  $this->pagination->create_html(url::create_url("default", "user", "history"));
        foreach($this->items as $key => $value)
        {
            $sum = 0;
            $ship = 50;
            $books = json_decode($value["books"]);
            $prices = json_decode($value["prices"]);
            $quantities = json_decode($value["quantities"]);
            $names = json_decode($value["names"]);
            $pictures = json_decode($value["pictures"]);
            $header = '<h3> Ma don hang: '.$value["id"].' - Thoi gian: '.date("H:i d/m/Y",strtotime($value["date"])).'
                        <span  style = "color:green;">&nbsp;&nbsp;RECEIVED</span></h3>';
            if($value["canceled"] == 1)
            {
                $header = '<h3> Ma don hang: '.$value["id"].' - Thoi gian: '.date("H:i d/m/Y",strtotime($value["date"])).' 
                            <span  style = "color:red;">&nbsp;&nbsp;CANCELED</span></h3>';
            }
            $xhtml .= '<div class="history-cart">
                            '.$header.'
                            <table class="cart_table">
                                <tr class="cart_title">
                                    <td>Item pic</td>
                                    <td>Book name</td>
                                    <td>Unit price (VND) </td>
                                    <td>Qty</td>
                                    <td>Total (VND) </td>
                                </tr>';
            foreach($books as $key_b => $value_b)
            {
                $picture = (!empty($pictures[$key_b]))?$pictures[$key_b]:"default_img.jpg";
                $image_path = TEMPLATE_FILE_PATH."book/".$picture;
                $link = url::create_url("default","book","detail",array("book_id" => $value_b));
                $total_price_per_unit = ($prices[$key_b] * $quantities[$key_b]);
                $sum += $total_price_per_unit;
                $xhtml .= '     <tr>
                                    <td><a href="'.$link.'">
                                        <img src="'.$image_path.'" alt="" title="" border="0" class="cart_thumb" />
                                    </a></td>
                                    <td>'.$names[$key_b].'</td>
                                    <td>'. number_format($prices[$key_b]).'</td>
                                    <td>'.$quantities[$key_b].'</td>
                                    <td>'.number_format($total_price_per_unit).'</td>
                                </tr>';
            }
                $xhtml .=
                            '<tr>
                                    <td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
                                    <td>' . number_format($ship) . '</td>
                                </tr>

                                <tr>
                                    <td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
                                    <td>' . number_format($sum) . '</td>
                                </tr></table>
                                                </div>
                                            <div class="clear"></div>';
                  
        }
    }
?>
<div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>bullet1.gif" alt="" title="" /></span>History</div>
<form action="#" method="post" name="adminForm" id="adminForm">
<div class="feat_prod_box_details">
    <?php echo $xhtml; ?>
    
</div>
<div>
    <input type="hidden" name="filter_page" value="1">
</div>
</form>
<div class="container">
    <?php echo $pagination_html; ?>
</div>