<?php 
    $link_change_pass = url::create_url("default","user","change_pass");
    $link_cart = url::create_url("default","user","cart");
    $link_history = url::create_url("default","user","history");
    $link_purchase_order = url::create_url("default","user","purchased");
    $message = "";
    if(isset($_SESSION["message_save_success"]))
    {
        $message .= '<div class = "message_save">'.$_SESSION["message_save_success"].'</div>';
        Session::delete("message_save_success");
    }
?>
<?php echo $message ?>
<div class="new_products">        
        <div class="new_prod_box">
            <a href="#">CHANGE PASSWORD</a>
            <div class="new_prod_bg">
                <a href="<?php echo $link_change_pass ?>"><img height="80" width="80" src="<?php echo $link_img ?>change_password.png" alt="" title="" class="thumb" border="0" /></a>
            </div>
        </div>
        <div class="new_prod_box">
            <a href="#">CART</a>
            <div class="new_prod_bg">
                <a href="<?php echo $link_cart ?>"><img  src="<?php echo $link_img ?>cart.png" alt="" title="" class="thumb" border="0" /></a>
            </div>
        </div>
        <div class="new_prod_box">
            <a href="#">HISTORY</a>
            <div class="new_prod_bg">
                <a href="<?php echo $link_history ?>"><img height="80" width="80" src="<?php echo $link_img ?>order.png" alt="" title="" class="thumb" border="0" /></a>
            </div>
        </div>
        <div class="new_prod_box">
            <a href="#">PURCHASED ORDER</a>
            <div class="new_prod_bg">
                <a href="<?php echo $link_purchase_order ?>"><img height="80" width="80" src="<?php echo $link_img ?>purchase_order.png" alt="" title="" class="thumb" border="0" /></a>
            </div>
        </div>
</div>