<div class="title"><span class="title_icon"><img src="<?php echo $link_img; ?>bullet1.gif" alt="" title="" /></span>My cart</div>
<?php 
    $xhtml = "";
    $ship_total_html = "";
    $link_continue = url::create_url("default","index","index"); 
    $link_check_out = url::create_url("default","user","buy");
    if(!empty($this->items))
    {
        $sum = 0;
        $ship = 50;
        foreach($this->items as $key => $value)
        {
            $sum += $value["total_price_per_unit"];
            $picture = (!empty($value["picture"]))?$value["picture"]:"default_img.jpg";
            $image_path = TEMPLATE_FILE_PATH."book/".$picture;
            $link = url::create_url("default","book","detail",array("book_id" => $value["id"]));
            $link_add = url::create_url("default","user","change_quantity",array("id" => $value["id"],"quantity" => $value["quantity"] ,"task"=>"add"));
            $link_sub = url::create_url("default","user","change_quantity",array("id" => $value["id"],"quantity" => $value["quantity"],"task"=>"sub"));
            $xhtml .= '<tr>
                        <td><a href="'.$link.'"><img src="'.$image_path.'" alt="" title="" border="0" class="cart_thumb" /></a></td>
                        <td><a href="'.$link.'" style = "text-decoration:none; color:black;">'.$value["name"].'</a></td>
                        <td>'. number_format($value["price_unit"]) .'</td>
                        <td> <a href = "javascript:change_quantity(\''.$link_sub.'\')" class="title_icon"><img weight = "12px" height = "12px" src="'. $link_img.'sub.png" alt="" title="" /></a>
                                    '.$value["quantity"].' 
                            <a href = "javascript:change_quantity(\''.$link_add.'\')" class="title_icon"><img weight = "12px" height = "12px" src="'. $link_img.'add.png" alt="" title="" /></a>
                        </td>
                        <td>'. number_format($value["total_price_per_unit"]) .'</td>
                    </tr>
                    <input type="hidden" name="form[books][]" value="'.$value["id"].'">
                    <input type="hidden" name="form[prices][]" value="'.$value["price_unit"].'">
                    <input type="hidden" name="form[quantities][]" value="'.$value["quantity"].'">
                    <input type="hidden" name="form[names][]" value="'.$value["name"].'">
                    <input type="hidden" name="form[pictures][]" value="'.$value["picture"].'">
                    ';
        }
        $sum += $ship;
        $ship_total_html = '<tr>
                                <td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
                                <td>'. number_format($ship).' VND </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
                                <td>'.number_format($sum).' VND </td>
                            </tr>';
    }
    $error_noti = "";
    if(!empty($this->errors))
    {
        foreach($this->errors as $key =>  $value)
        {
            $error_noti .= '<div class="error-public">'.$key.' : '.$value.'</div>';
        }
    }
    $phone = ($_SESSION["user"]["info"]["phone"] != null)?$_SESSION["user"]["info"]["phone"]:"";
    if((isset($this->result["phone"])) && ($this->result["phone"] != null))
    {
        $phone = $this->result["phone"];
    }
    $address = ($_SESSION["user"]["info"]["address"] != null)?$_SESSION["user"]["info"]["address"]:"";
    if((isset($this->result["address"])) && ($this->result["address"] != null))
    {
        $address = $this->result["address"];
    }
?>
<div class="feat_prod_box_details">
    <?php echo $error_noti; ?>
    <form action="#" method="POST" id = "adminForm">
        <div class="form_user_cart">
            <div class="form_row">
                <label class="contact"><strong>Full name:</strong></label>
                <input type="text" class="contact_input"  name="form[order_person]" value="<?php echo $_SESSION["user"]["info"]["full_name"]; ?>" />
            </div>
            <div class="form_row">
                <label class="contact"><strong>Phone:</strong></label>
                <input type="text" class="contact_input" name="form[phone]" value="<?php echo $phone ?>" />
            </div>
            <div class="form_row ">
                <label class="contact"><strong>Address:</strong></label>
                <input type="text" class="contact_input" name="form[address]" value="<?php echo $address; ?>" />
            </div>
            <div class="form_row">
            </div>
        </div>
        <table class="cart_table">
            <tr class="cart_title">
                <td>Item pic</td>
                <td>Book name</td>
                <td>Unit price (VND) </td>
                <td>Qty</td>
                <td>Total (VND) </td>
            </tr>
            <?php echo $xhtml . $ship_total_html; ?>
        </table>
        <a href="<?php echo $link_continue; ?>" class="continue">&lt; continue</a>
        <a href="#" onclick = "javascript:submit_form('<?php echo $link_check_out;?>')"class="checkout">checkout &gt;</a>   
    </form>
    
</div>
<div class="clear"></div>