<div class="title"><span class="title_icon"><img src="<?php echo $link_img?>bullet1.gif" alt="" title="" /></span>Dang Nhap</div>

<div class="feat_prod_box_details">
    <div class="contact_form">
        <div class="form_subtitle">Login Account</div>
        
        <?php
            if(isset($this->error_message))
            {
                echo '<div class="error-public">'.$this->error_message.'</div>';
            }    
        ?>
        
        
        <form name="register" action="#" method="POST">
            <div class="form_row">
                <label class="contact"><strong>Email:</strong></label>
                <input type="text" class="contact_input"  name="form[email]" value="<?php echo @$this->result["email"]; ?>" />
            </div>
            <div class="form_row">
                <label class="contact"><strong>Password:</strong></label>
                <input type="password" class="contact_input" name="form[password]" value="<?php echo @$this->result["password"]; ?>" />
            </div>
            
            <div class="form_row">
                <input type="hidden" name="form[token]" value="<?php echo time(); ?>">
                <input type="submit" class="register" name="form[submit]" value="login" />
            </div>
        </form>
    </div>

</div>
<div class="clear"></div>