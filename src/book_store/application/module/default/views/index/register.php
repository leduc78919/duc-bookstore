<div class="title"><span class="title_icon"><img src="<?php echo $link_img?>bullet1.gif" alt="" title="" /></span>Register</div>

<div class="feat_prod_box_details">
    <div class="contact_form">
        <div class="form_subtitle">create new account</div>
        <?php
            echo helper::cms_erros(@$this->errors);
           
        ?>
        <form name="register" action="#" method="POST">
            <div class="form_row">
                <label class="contact"><strong>Username:</strong></label> 
                <input type="text" name = "form[user_name]" class="contact_input" value="<?php echo @$this->result["user_name"]; ?>"/>
            </div>
            <div class="form_row">
                <label class="contact"><strong>Fullname:</strong></label>
                <input type="text" class="contact_input" name="form[full_name]" value="<?php echo @$this->result["full_name"]; ?>"/>
            </div>
            <div class="form_row">
                <label class="contact"><strong>Password:</strong></label>
                <input type="password" class="contact_input" name="form[password]" value="<?php echo @$this->result["password"]; ?>" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Email:</strong></label>
                <input type="text" class="contact_input"  name="form[email]" value="<?php echo @$this->result["email"]; ?>" />
            </div>
            <div class="form_row">
                <label class="contact"><strong>Phone:</strong></label>
                <input type="text" class="contact_input"  name="form[phone]" value="<?php echo @$this->result["phone"]; ?>" />
            </div>
            <div class="form_row">
                <label class="contact"><strong>Address:</strong></label>
                <input type="text" class="contact_input"  name="form[address]" value="<?php echo @$this->result["address"]; ?>" />
            </div>
            <div class="form_row">
                <div class="terms">
                    <input type="checkbox" name="terms" />
                    I agree to the <a href="#">terms &amp; conditions</a>
                </div>
            </div>


            <div class="form_row">
                <input type="hidden" name="form[token]" value="<?php echo time(); ?>">
                <input type="submit" class="register" name="form[submit]" value="register" />
            </div>
        </form>
    </div>

</div>
<div class="clear"></div>