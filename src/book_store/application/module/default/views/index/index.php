<div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>bullet1.gif" alt="" title="" /></span>Featured books</div>
<form action="#" method="post" name="adminForm" id="adminForm">
    <div class="filter">
        <fieldset id="filter-bar">
            <div class="filter-search fltlft">
                <label class="filter-search-lbl" for="filter_search">Filter:</label>
                <input type="text" name="filter_search" id="filter_search" value="<?php echo @$this->params["filter_search"]; ?>" title="Search in module title.">
                <button type="submit" name="filter_submit">Search</button>
                <button type="button" name="filter_clear">Clear</button>
            </div>
        </fieldset>
    </div>
<?php
    $pagination_html =  $this->pagination->create_html(url::create_url("default", "index", "index"));
    foreach ($this->items as $key => $value) {
        $picture = (!empty($value["picture"])) ? $value["picture"] : "default_img.jpg";
        $image_path = TEMPLATE_FILE_PATH . "book/" . $picture;
        $description = (strlen($value["description"]) > 200) ? substr($value["description"], 0, 200) . "..." : $value["description"];
        $page = (isset($this->params["filter_page"]))?$this->params["filter_page"]:1;
        $link = url::create_url("default", "book", "detail", array("book_id" => $value["id"],"page" => $page));

?>
        <div class="feat_prod_box">
            <div class="prod_img">
                <a href="<?php echo $link ?>"><img width="98" height="150" src="<?php echo $image_path; ?>" alt="" title="" border="0" /></a>
            </div>

            <div class="prod_det_box">
                <div class="box_top"></div>
                <div class="box_center">
                    <div class="prod_title"><a style="color:black; font-weight:bold; color:chocolate; text-decoration:none " href="<?php echo $link ?>"><?php echo $value["name"] ?></a></div>
                    <p class="details"><?php echo $description ?></p>
                    <a href="<?php echo $link ?>" class="more">- more details -</a>
                    <div class="clear"></div>
                </div>

                <div class="box_bottom"></div>
            </div>
            <div class="clear"></div>
        </div>
    <?php
    }
    ?>
    <div>
        <input type="hidden" name="filter_page" value="1">
    </div>
</form>
<div class="container">
    <?php echo $pagination_html; ?>
</div>
<div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>bullet2.gif" alt="" title="" /></span>New books</div>

<div class="new_products">
    <?php
    foreach ($this->new_books as $key => $value) {
        $picture = (!empty($value["picture"])) ? $value["picture"] : "default_img.jpg";
        $image_path = TEMPLATE_FILE_PATH . "book/" . $picture;
        $name = (strlen($value["name"]) > 20) ? substr($value["name"], 0, 20) . "..." : $value["name"];
        $link = url::create_url("default", "book", "detail", array("book_id" => $value["id"]));
    ?>
        <div class="new_prod_box">
            <a style="color:black; font-weight:bold; " href="<?php echo $link ?>"><?php echo $name ?></a>
            <div class="new_prod_bg">
                <span class="new_icon"><img src="<?php echo $link_img ?>new_icon.gif" alt="" title="" /></span>
                <a href="<?php echo $link ?>"><img src="<?php echo $image_path ?>" alt="" title="" class="thumb" border="0" /></a>
            </div>
        </div>
    <?php
    }
    ?>
</div>