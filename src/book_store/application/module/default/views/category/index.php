<?php 
    $pagination_html =  $this->pagination->create_html(url::create_url("default","category","index"));
    if(!empty($this->items))
    {
        $xhtml = "";
        foreach($this->items as $value)
        {
            $link = url::create_url("default","book","list",array("category_id" => $value["id"]));
            $picture = (!empty($value["picture"]))?$value["picture"]:"default_img.jpg";
            $path = TEMPLATE_FILE_PATH . "category/" .$picture;
            $xhtml .= '<div class="new_prod_box">
                    <a href="'.$link.'">'.$value["name"].'</a>
                    <div class="new_prod_bg">
                        <a href="'.$link.'"><img src="'.$path.'" alt="" title="" class="thumb" border="0" /></a>
                    </div>
                </div>';
        }
    }
?>
<div class="new_products">
    <form action="#" method="post" name="adminForm" id="adminForm">
        <?php 
            echo $xhtml;
        ?>
        <div>
            <input type="hidden" name="filter_page" value="1">
        </div>
    </form>
    <div class="container">
       <?php echo $pagination_html; ?>
    </div>
</div>