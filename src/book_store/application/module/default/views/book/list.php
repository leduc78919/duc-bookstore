<div class="title"><span class="title_icon"><img src="<?php echo $link_img ?>bullet1.gif" alt="" title="" /></span><?php echo $this->name_category; ?></div>

<?php 
foreach($this->items as $value)
{
    $picture = (!empty($value["picture"]))?$value["picture"]:"default_img.jpg";
    $image_path = TEMPLATE_FILE_PATH."book/".$picture;
    $description = (strlen($value["description"]) > 200)?substr($value["description"],0,200)."...":$value["description"];
    $link = url::create_url("default","book","detail",array("book_id" => $value["id"],"category_id" => $this->params["category_id"]));
    $price = '<span class="red">'.number_format($value['price']).'VND</span>';
    if($value["sale_off"] > 0)
    {
        $price_sale_off = $value["price"] - (($value["sale_off"]/100)*$value["price"]);
        $price = '<span class="red-through">'.number_format($value['price']).'VND</span>';
        $price .= '<span class="red">'.number_format($price_sale_off).'VND </span>';
    }
?>
<div class="feat_prod_box">
    <div class="prod_img"><a href="<?php echo $link; ?>"><img width="98" height="150" src="<?php echo $image_path; ?>" alt="" title="" border="0" /></a>
        <br /><br />
    </div>

    <div class="prod_det_box">
        <div class="box_top"></div>
        <div class="box_center">
            <div class="prod_title"><a style="color:black; font-weight:bold; color:chocolate; text-decoration:none " href="<?php echo $link ?>"><?php echo $value["name"] ?></a></div>
            <p  class="details"><?php echo  $description ; ?></p>
            <div class="price"><strong>PRICE:</strong> <span class="red"><?php echo $price; ?></span></div>
            <a href="<?php echo $link; ?>" class="more">- more details -</a>
            <div class="clear"></div>
        </div>

        <div class="box_bottom"></div>
    </div>
    <div class="clear"></div>
</div>
<?php } ?>