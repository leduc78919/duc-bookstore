<?php
$value = $this->info;
$picture = (!empty($value["picture"])) ? $value["picture"] : "default_img.jpg";
$origin_picture = (!empty($value["picture"])) ? str_replace("60x90-", "", $value["picture"]) : "default_img.jpg";
$image_path = TEMPLATE_FILE_PATH . "book/" . $picture;
$origin_image_path = TEMPLATE_FILE_PATH . "book/" . $origin_picture;
$description = (strlen($value["description"]) > 500) ? substr($value["description"], 0, 500) . "..." : $value["description"];
$price = '<span class="red">' . number_format($value['price']) . 'VND</span>';
$price_real = $value["price"];
if ($value["sale_off"] > 0) {
    $price_real = $value["price"] - (($value["sale_off"] / 100) * $value["price"]);
    $price = '<span class="red-through">' . number_format($value['price']) . 'VND</span>';
    $price .= '<span class="red">' . number_format($price_real) . 'VND </span>';
}
$link = url::create_url("default", "user", "order", array("book_id" => $value["id"], "price" => $price_real));
$link_back_page = url::create_url("default", "index", "index");
if(isset($this->params["category_id"]))
{
    $link_back_page = url::create_url("default", "book", "list",array("category_id" => $this->params["category_id"]));
}
?>
<form action="#" id = "adminForm" method="POST">
    <div class="title">
        <span class="title_icon">
            <a onclick="javascript:submit_form('<?php echo $link_back_page; ?>')" href="#">
                <img width="26px" height="26px" src="<?php echo $link_img ?>previous.png" alt="" title="" />
            </a>
        </span>
        <?php echo $value["name"] ?>
    </div>

    <div class="feat_prod_box_details">

        <div class="prod_img"><a href="<?php echo $link ?>"><img width="98" height="150" src="<?php echo $image_path ?>" alt="" title="" border="0" /></a>
            <br /><br />
            <a data-fancybox="gallery" href="<?php echo $origin_image_path ?>" rel="lightbox"><img src="<?php echo $link_img ?>zoom.gif" alt="" title="" border="0" /></a>
        </div>
        <div class="prod_det_box">
            <div class="box_top"></div>
            <div class="box_center">
                <div class="prod_title"><?php echo $value["name"] ?></div>
                <p class="details"><?php echo $description ?></p>
                <div class="price">
                    <strong>PRICE:</strong>
                    <span class="red"><?php echo $price; ?></span>
                </div>
                <a href="<?php echo $link ?>" class="more"><img src="<?php echo $link_img ?>order_now.gif" alt="" title="" border="0" /></a>
                <div class="clear"></div>
            </div>

            <div class="box_bottom"></div>
        </div>
        <div class="clear"></div>
    </div>
    <?php
        if(isset($this->params["page"]))
        {
           echo '<input type="hidden" name="filter_page" value="'.$this->params["page"].'">';
        } 
    ?>
    
</form>

<?php
$xhtml = "";
if (!empty($this->related_books)) {

    foreach ($this->related_books as $key => $value_related) {
        $link = url::create_url("default", "book", "detail", array("book_id" => $value_related["id"]));
        $picture = (!empty($value_related["picture"])) ? $value_related["picture"] : "default_img.jpg";
        $image_path = TEMPLATE_FILE_PATH . "book/" . $picture;
        $xhtml .= ' <div class="new_prod_box">
                            <a href="' . $link . '">' . $value_related["name"] . '</a>
                            <div class="new_prod_bg">
                                <a href="' . $link . '"><img src="' . $image_path . '" alt="" title="" class="thumb" border="0" /></a>
                            </div>
                        </div>';
    }
}
?>
<div id="demo" class="demolayout">

    <ul id="demo-nav" class="demolayout">
        <li><a class="tab1 active" href="#">More details</a></li>
        <li><a class="tab2" href="#">Related books</a></li>
    </ul>

    <div class="tabs-container">
        <div style="display: block;" class="tab" id="tab1">
            <p class="more_details"><?php echo $value["description"]; ?></p>
        </div>

        <div style="display: none;" class="tab" id="tab2">
            <?php echo $xhtml; ?>
            <div class="clear"></div>
        </div>

    </div>


</div>



<div class="clear"></div>