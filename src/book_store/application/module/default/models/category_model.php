<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class category_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_CATEGORY;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0 AND status = 1';
        $result = $this->single_record($query);
        return $result["total"];
    }

    public function get_items($array,$options = null)
    {
        $query[] = 'SELECT *';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0 AND status = 1';
        //PAGINATION
        $pagination = $array["pagination"];
        $items_per_page = $pagination["items_per_page"];
        if($items_per_page > 0)
        {
            $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
            $query[] .= "LIMIT $position,$items_per_page";
        }
        // echo $query = implode(" ",$query);
        $result = $this->list_record($query);
        return $result;
    }
}