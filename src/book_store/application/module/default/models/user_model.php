<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class user_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_USER;
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "save_cart")
        {
            $id = $this->random_name(7);
            $order_person = $params["form"]["order_person"];
            $phone = "(+84)".$params["form"]["phone"];
            $address = $params["form"]["address"];
            $user_id = $_SESSION["user"]["info"]["id"];
            $books = json_encode($params["form"]["books"]);
            $prices = json_encode($params["form"]["prices"]);
            $quantities = json_encode($params["form"]["quantities"]);
            $names = json_encode($params["form"]["names"]);
            $pictures = json_encode($params["form"]["pictures"]);
            $status = 0;
            $date = date("Y-m-d H:i:s",time());
            $query = "INSERT INTO `".DB_TABLE_CART."`(id,user_id,books,prices,quantities,names,pictures,status,date,order_person,phone,address) 
                    VALUES('$id',$user_id,'$books','$prices','$quantities',
                    '$names','$pictures',$status,'$date','$order_person','$phone','$address')";
            $this->query($query);
            $this->delete_pre_order($user_id);
            Session::delete("cart");
            Session::set("message_save_success","Order has been placed successfully");
            helper::redirect("default","user","index");
        }

        if($options["task"] == "save_pre_order")
        {
            $user_id = $params["user_id"];
            $check_id = $this->check_id_pre_order();
            $books = json_encode($params["books"]);
            $prices = json_encode($params["prices"]);
            $quantities = json_encode($params["quantities"]);
            if(in_array($user_id,$check_id))
            {
                $query = "UPDATE `".DB_TABLE_PRE_ORDER."` SET books = '$books',prices ='$prices',quantities = '$quantities'
                            WHERE user_id = $user_id";
                            
            }
            else
            {
                $query = "INSERT INTO `".DB_TABLE_PRE_ORDER."`(user_id,books,prices,quantities) 
                    VALUES($user_id,'$books','$prices','$quantities')";
            }
            $this->query($query);
            Session::delete("cart");
        }

        if($options["task"] == "change_pass")
        {
            $query[] = 'UPDATE `'.DB_TABLE_USER.'` SET password = "'.$params["password"].'",modified = "'.$params["modified"].'",
                        modified_by = "'.$params["modified_by"].'" WHERE id = '. $params["modified_by"];
            $this->query($query);
            $_SESSION["user"]["info"]["password"] = $params["password"];
            Session::set("message_change_pass_success","Password changed successful");
        }
    }

    public function check_id_pre_order()
    {
        $query[] = "SELECT user_id";
        $query[] = "FROM `".DB_TABLE_PRE_ORDER."`";
        $result = $this->single_record($query);
        return $result;
    }

    public function single_item($params,$options = null)
    {
        // $query[] = 'SELECT u.id,u.user_name,u.email,u.full_name,u.status,u.ordering,u.password,u.created,u.created_by,u.modified,u.modified_by,g.id as group_id';
        // $query[] = 'FROM `'.$this->table.'` as u,`'.DB_TABLE_GROUP.'` as g';
        // $query[] = 'WHERE u.group_id = g.id AND u.id ='. $params["id"].'';
        // $result = $this->single_record($query);
        // return $result; 
    }

    public function info_items($params,$options = null)
    {
        if($options["type"] == "get_info_cart")
        {
            if(!empty($_SESSION["cart"]))
            {
                $id_list = "(";
                foreach($_SESSION["cart"]["quantity"] as $key => $value)
                {
                    $id_list .= $key.",";
                }
                $id_list .= "0)";
                $query[] = 'SELECT id,name,picture';
                $query[] = 'FROM `'. DB_TABLE_BOOK .'`';
                $query[] = 'WHERE id IN'.$id_list;
                $result = $this->list_record($query);
                foreach($result as $key => $value)
                {
                    $result[$key]["quantity"]                = $_SESSION["cart"]["quantity"][$value["id"]];
                    $result[$key]["total_price_per_unit"]    = $_SESSION["cart"]["price"][$value["id"]];
                    $result[$key]["price_unit"]              = $result[$key]["total_price_per_unit"]/$result[$key]["quantity"];
                }
                return $result;
            }
        }
    }

    public function list_items($params,$options = null)
    {
        if($options["task"] == "get_history_cart")
        {
            $user = $_SESSION["user"]["info"]["id"];
            $query[] = "SELECT id,user_id,books,prices,quantities,names,pictures,date,canceled";
            $query[] = "FROM `".DB_TABLE_CART ."`";
            $query[] = "WHERE user_id = ".$user." AND (status = 1 OR canceled = 1) AND confirmed = 1";
            $query[] = "ORDER BY date DESC";
            //PAGINATION
            $pagination = $params["pagination"];
            $items_per_page = $pagination["items_per_page"];
            if($items_per_page > 0)
            {
                $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
                $query[] .= "LIMIT $position,$items_per_page";
            }
            
            $result = $this->list_record($query);
            return $result;
        }

        if($options["task"] == "get_purchased_order")
        {
            $user = $_SESSION["user"]["info"]["id"];
            $query[] = "SELECT id,user_id,books,prices,quantities,names,pictures,date,order_person,phone,address";
            $query[] = "FROM `".DB_TABLE_CART ."`";
            $query[] = "WHERE user_id = ".$user." AND status = 0 AND canceled = 0";
            $query[] = "ORDER BY date DESC";
            //PAGINATION
            $pagination = $params["pagination"];
            $items_per_page = $pagination["items_per_page"];
            if($items_per_page > 0)
            {
                $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
                $query[] .= "LIMIT $position,$items_per_page";
            }
            $result = $this->list_record($query);
            return $result;
        }
    }

    public function delete_pre_order($user_id,$options = null)
    {
        $query = "DELETE FROM `".DB_TABLE_PRE_ORDER."` WHERE user_id = ".$user_id;
        $this->query($query);  
    }

    public function verify_order($params,$options = null)
    {
        $status = 0;
        $canceled = 0;
        if(isset($params["status"]))
        {
            $status = 1;
        }
        else if(isset($params["canceled"]))
        {
            $canceled = 1;
        }
        $query[] = 'UPDATE `'.DB_TABLE_CART.'` SET status = '. $status .',canceled = '.$canceled;
        $query[] = 'WHERE id = "'. $params["id_order"].'" AND user_id ='.$params["user_id"];
        $this->query($query);
    }

    public function count_items($array,$options = null)
    {
        $user = $_SESSION["user"]["info"]["id"];
        $query[] = 'SELECT COUNT(user_id) AS total';
        $query[] = 'FROM `'.DB_TABLE_CART.'`';
        $query[] = 'WHERE  user_id = '.$user;
       
        //SEARCH
        if($options["task"] == "history")
        {
            $query[] .= 'AND (status = 1 OR canceled = 1 ) AND confirmed = 1';
            
        }
        if($options["task"] == "purchased")
        {
            $query[] .= 'AND status = 0 AND canceled = 0 ';
            
        }
        // echo $query = implode(" ",$query);
        // echo "<br/>";
        $result = $this->single_record($query);
        return $result["total"];
    }
    
    public function change_quantity($params,$options = null)
    {
        $quantity = $params["quantity"];
        $book_id     = $params["id"];
        $price    = $_SESSION["cart"]["price"][$book_id]/$quantity;
        if($params["task"] == "add")
        {
            $quantity += 1;
            $_SESSION["cart"]["quantity"][$book_id] = $quantity;
            $_SESSION["cart"]["price"][$book_id] +=  $price;
            return 1;
        }
        else if($params["task"] = "sub")
        {
            $quantity -= 1;
            if($quantity == 0)
            {
                $temp = $_SESSION["cart"];
                unset($temp["quantity"][$book_id]);
                unset($temp["price"][$book_id]);
                Session::delete("cart");
                Session::set("cart",$temp);
                return 1;
            }
            else
            {
                $_SESSION["cart"]["quantity"][$book_id] = $quantity;
                $_SESSION["cart"]["price"][$book_id] -=  $price;
                return 1;
            }
        }
    }
}
