<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class book_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_BOOK;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.$this->table.'`';
        $query[] = 'WHERE id > 0 AND status = 1 AND category_id = '.$array["category_id"];
        $result = $this->single_record($query);
        return $result["total"];
    }

    public function get_items($array,$options = null)
    {
        if($options["task"] == "list_from_category")
        {
            $query[] = 'SELECT *';
            $query[] = 'FROM `'.$this->table.'`';
            $query[] = 'WHERE id > 0 AND status = 1 AND category_id = '.$array["category_id"];
            //PAGINATION
            $pagination = $array["pagination"];
            $items_per_page = $pagination["items_per_page"];
            if($items_per_page > 0)
            {
                $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
                $query[] .= "LIMIT $position,$items_per_page";
            }
            // echo $query = implode(" ",$query);
            $result = $this->list_record($query);
            return $result;
        }
        if($options["task"] == "related_books")
        {
            $query[] = 'SELECT *';
            $query[] = 'FROM `'.$this->table.'`';
            $query[] = 'WHERE status = 1 AND category_id ='.$options["category_id"].' AND id <> '.$options["book_id"];
            $query[] = 'LIMIT 0,6';
            $result = $this->list_record($query);
            return $result;
        }
       
    }

    public function info_items($array,$options = null)
    {
        if($options["task"] == "get_name_catagory")
        {
            $query[] = 'SELECT name';
            $query[] = 'FROM `'.DB_TABLE_CATEGORY.'`';
            $query[] = 'WHERE id = '. $array["category_id"];
            $result = $this->single_record($query);
            return $result["name"];      
        }
        if($options["task"] == "get_info_detail")
        {
            $query[] = 'SELECT id,name,category_id,picture,description,price,sale_off';
            $query[] = 'FROM `'.DB_TABLE_BOOK.'`';
            $query[] = 'WHERE id = '. $array["book_id"];
            $result = $this->single_record($query);
            return $result;  
        }
         
    }

    public function item_in_box($params,$options = null)
    {
        $query[] = 'SELECT id,name';
        $query[] = 'FROM `'.DB_TABLE_CATEGORY.'`';
        $result = $this->fetch_pairs($query);
        return $result;
    }
}