<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class index_model extends Model_db
{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_USER;
    }

    public function save_item($params,$options = null)
    {
        if($options["task"] == "add")
        {
            $this->insert_db($params);
            Session::set("message",array("class" => "success","content" => 'Register has been success!'));
            return $this->last_id();
            
        }
        else if($options["task"] == "edit")
        {
           $where = array(array("id",$options["id"]));
           $this->update_db($params,$where);
           return $options["id"];
        }
    }

    public function single_item($params,$options = null)
    {
        $query[] = 'SELECT u.id,u.user_name,u.email,u.full_name,u.password,u.status,u.ordering,u.password,u.phone,u.address,g.id as group_id,g.group_acp';
        $query[] = 'FROM `'.$this->table.'` as u LEFT JOIN `'.DB_TABLE_GROUP.'` as g';
        $query[] = 'ON u.group_id = g.id WHERE u.id ='. $params["id"];
        $result = $this->single_record($query);
        return $result; 
    }

    public function check_login($options)
    {
        $query    = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE email = "'.$options['email'].'" AND password = "'.$options['password'].'"';
        $list = $this->single_record($query);
        return $list;
    }

    public function count_items($array,$options = null)
    {
        $query[] = 'SELECT COUNT(id) AS total';
        $query[] = 'FROM `'.DB_TABLE_BOOK.'`';
        $query[] = 'WHERE id > 0 AND status = 1';
       
        //SEARCH
        if(!empty($array["filter_search"]))
        {
            $search = $array["filter_search"];
            $query[] .= 'AND name LIKE \'%'.$search.'%\'';
            
        }
        // echo $query = implode(" ",$query);
        // echo "<br/>";
        $result = $this->single_record($query);
        return $result["total"];
    }


    public function get_items($array,$options = null)
    {
        if($options["task"] == "book_special")
        {
            $query[] = 'SELECT id,name,picture,description,special';
            $query[] = 'FROM `'.DB_TABLE_BOOK.'`';
            $query[] = 'WHERE id > 0 AND status = 1';
            //SEARCH
            if(!empty($array["filter_search"]))
            {
                $search = $array["filter_search"];
                $query[] .= 'AND name LIKE \'%'.$search.'%\'';
                
            }
            //PAGINATION
            $pagination = $array["pagination"];
            $items_per_page = $pagination["items_per_page"];
            if($items_per_page > 0)
            {
                $position = ((int)$pagination["current_page"] - 1)*$items_per_page;
                $query[] .= "LIMIT $position,$items_per_page";
            }
                // echo $query = implode(" ",$query);
                $result = $this->list_record($query);
                return $result;
            }

        if($options["task"] == "book_new")
        {
            $query[] = 'SELECT id,name,picture';
            $query[] = 'FROM `'.DB_TABLE_BOOK.'`';
            $query[] = 'WHERE id > 0 AND status = 1';
            $query[] = 'ORDER BY id DESC';
            $query[] = 'LIMIT 0,3';
            // echo $query = implode(" ",$query);
            $result = $this->list_record($query);
            return $result;
        }
        
    }

    public function get_pre_order($params,$options = null)
    {
        $query[] = "SELECT books,prices,quantities";
        $query[] = "FROM `".DB_TABLE_PRE_ORDER."`";
        $query[] = "WHERE user_id = ". $params["id"];
        $result = $this->single_record($query);
        return $result;
    }
}