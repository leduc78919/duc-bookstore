<?php 
class   category_controller extends controller
{
    private $_columns = array("id","name","status","picture","ordering","created","created_by","modified","modified_by");
    public function __construct()
    {
       parent::__construct();
    }


    // hien thi danh sach group
    public function index_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        //$this->view->set_title("Category ");
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 9,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items = $this->db->get_items($this->params,null);
        $this->view->render("category/index");
    }

    
}