<?php 
class   book_controller extends controller
{
    private $_columns = array("id","name","status","picture","ordering","created","created_by","modified","modified_by",
    "price","sale_off","description","special","category_id");
    public function __construct()
    {
       parent::__construct();
    }

    public function list_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        //$this->view->set_title("Category ");
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 9,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->category_box = $this->db->item_in_box($this->params);
        if(!in_array($this->params["category_id"],array_flip($this->category_box)))
        {
            helper::redirect("default","index","notice",array("type" => "invalid_category"));
        }
        $this->view->items = $this->db->get_items($this->params,array("task" => "list_from_category"));
        $this->view->name_category = $this->db->info_items($this->params,array("task" => "get_name_catagory"));
        $this->view->render("book/list");
    }

    public function detail_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $this->view->info = $this->db->info_items($this->params,array("task" => "get_info_detail"));
        if(empty($this->view->info))
        {
            helper::redirect("default","index","notice",array("type" => "invalid_id"));
        }
        $this->view->related_books = $this->db->get_items($this->params,array("task" => "related_books",
                                                                            "category_id" => $this->view->info["category_id"],
                                                                            "book_id" => $this->view->info["id"]));
        $this->view->render("book/detail");
    }


    
}