<?php 
class error_controller extends controller
{
    public function __construct()
    {
       parent::__construct();
    }
    public function index_action()
    {
        $this->view->render("error/index");
    }
}