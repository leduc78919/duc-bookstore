<?php
class index_controller extends controller
{
    private $_columns = array("id","user_name","email","full_name","password","created","created_by",
    "modified","modified_by","status","ordering","register_date","register_ip",
    "visited_date","visited_ip","group_id","phone","address");
    public function notice_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $this->view->render("index/notice");
    }

    public function index_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $total_items = $this->db->count_items($this->params);
        $config = array
        (
            "items_per_page"      => 3,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items     = $this->db->get_items($this->params,array("task" => "book_special"));
        $this->view->new_books = $this->db->get_items($this->params,array("task" => "book_new"));
        $this->view->render("index/index");
    }

    public function register_action()
    {
        if($logged = (isset($_SESSION["user"]["login"]) && ($_SESSION["user"]["login"] == true) && ($_SESSION["user"]["time"] + TIME_LOGIN > time())))
        {
            helper::redirect("default","index","index");
        }
        $this->template_obj->set_all('default/main','index.php','template.ini');
        if(isset($this->params["form"]["submit"]))
        {
            if(isset($_SESSION["token"]) && $_SESSION["token"] == $this->params["form"]["token"])
            {
                Session::delete('token');
                Session::delete('message');
                helper::redirect("default","user","register");
            }
            else
            {
                Session::set("token",$this->params["form"]["token"]);
            }
            $this->params["form"]["phone"] = (int)$this->params["form"]["phone"];
            $validate = new validate($this->params["form"]);
            $query_user = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE user_name = "'.trim( $this->params["form"]["user_name"]).' "';
            $query_email = 'SELECT id FROM `'.DB_TABLE_USER.'` WHERE email     = "'.trim( $this->params["form"]["email"]).' "';
            $validate->addRule("user_name","string-exit", array("min" => 3, "max" => 255, "id" => "user_name","database"=>$this->db,"query" => $query_user))
                    ->addRule("full_name","string", array("min" => 3, "max" => 255, "id" => "full_name"))
                    ->addRule("email","email-exit",array("database"=>$this->db,"query" => $query_email))
                    ->addRule("password","password",array("id" => "password"))
                    ->addRule("phone","numeric", array("min" => 10000000, "max" => 9999999999999, "id" => "phone"))
                    ->addRule("address","string", array("min" => 3, "max" => 255, "id" => "address"));
            $validate->run();
            $this->view->result = $validate->showResult();
            if(!$validate->isValid())
            {
                $this->view->errors = $validate->showError();
            }
            else
            {
                $this->params["form"]["register_ip"] = $_SERVER['REMOTE_ADDR'];
                $this->params["form"]["status"] = 1;
                $this->params["form"]["register_date"] = date("Y-m-d H:m:s",time());
                $this->params["form"]["created"] = date("Y-m-d", time());
                $result_merge = array_intersect_key($this->params["form"],array_flip($this->_columns));
                $id_return = $this->db->save_item($result_merge,array("task" => "add"));
                helper::redirect("default","index","notice",array("type" => "user_register"));
                
            }
        }
        $this->view->render("index/register");
    }

    public function login_action()
    { 
        
        if($logged = (isset($_SESSION["user"]["login"]) && ($_SESSION["user"]["login"] == true) && ($_SESSION["user"]["time"] + TIME_LOGIN > time())))
        {
            helper::redirect("default","index","index");
        }
        $this->template_obj->set_all('default/main','index.php','template.ini');
        if(isset($this->params["form"]["token"]))
        {
            $email = $this->params["form"]["email"];
            $password  = $this->params["form"]["password"];
            $pattern = array('#<script ([^>]*)>#','#</script>#','#([^A-Za-z0-9@_\s\.] *)#');
            $replace = array('&lt;script //1&gt;','&lt;/script&gt;','    ');
            $email = preg_replace($pattern,$replace,$email);
            $password  = preg_replace($pattern,$replace,$password);
            $item = $this->db->check_login(array("email" => $email,"password" => $password));
            if(empty($item))
            {
                $this->view->error_message = "Email OR Password is not correct";
            }
            else
            {
                $info_user = $this->db->single_item($item);
                $list_pre_order = $this->db->get_pre_order($info_user);
                if(!empty($list_pre_order))
                {
                    $temp_cart = array();
                    $quantity = json_decode($list_pre_order["quantities"]);
                    $price = json_decode($list_pre_order["prices"]);
                    $book  = json_decode($list_pre_order["books"]);
                    foreach($book as $Key => $value)
                    {
                        $temp_cart["quantity"][$value] = $quantity[$Key];
                        $temp_cart["price"][$value] = $price[$Key];
                    }
                    Session::set("cart",$temp_cart);
                    // echo "<pre>";
                    // print_r($temp_cart);
                    // echo "</pre>";
                    // die();
                }
                $array_session = array(
                    "login" => true,
                    "info"  => $info_user,
                    "time"  => time(),
                    "group_acp" => $info_user["group_acp"]
                );
                Session::set("user",$array_session);
                // echo "<pre>";
                // print_r($_SESSION);
                // echo "</pre>";
                helper::redirect("default","index","index");
            }
        }
       
        $this->view->render("index/login");
    }
}