<?php 
class user_controller extends controller
{
    private $_columns = array("id","user_name","email","full_name","password","created","created_by",
                            "modified","modified_by","status","ordering","register_date","register_ip",
                            "visited_date","visited_ip","group_id");
    public function __construct()
    {
       parent::__construct();
    
    }

    public function logout_action()
    {
        if(isset($_SESSION["cart"]))
        {
            foreach($_SESSION["cart"]["quantity"] as $key => $value)
            {
                $this->params["books"][] = $key;
                $this->params["prices"][] = $_SESSION["cart"]["price"][$key];
                $this->params["quantities"][] = $value;
                $this->params["user_id"] = $_SESSION["user"]["info"]["id"];
            }
            $this->db->save_item($this->params,array("task" => "save_pre_order"));
        }
        Session::delete("user");
        helper::redirect("default","index","index");    
    }

    public function index_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $this->view->render("user/index");
    }

    public function order_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $cart = (isset($_SESSION["cart"]))?Session::get("cart"):array();
        $id = $this->params["book_id"];
        $price = $this->params["price"];
        if(empty($cart))
        {
            $cart["quantity"][$id] = 1;
            $cart["price"][$id]    = $price;
        }
        else
        {
            if(key_exists($id,$cart["quantity"]))
            {
                $cart["quantity"][$id] += 1;
                $cart["price"][$id]    = $price * $cart["quantity"][$id];
            }
            else
            {
                $cart["quantity"][$id] = 1;
                $cart["price"][$id]    = $price;
            }
        }
        Session::set("cart",$cart);
        helper::redirect("default","book","detail",array("book_id" => $id));
    }

    public function cart_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $this->view->items = $this->db->info_items($this->params,array("type" => "get_info_cart")); 
        $this->view->render("user/cart");
    }

    public function buy_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        if(isset($this->params["form"]["books"]))
        {
             if((!empty($this->params["form"])) && ($this->params["form"]["books"] != null))
            {
                $this->params["form"]["phone"] = (int)$this->params["form"]["phone"];
                $validate = new validate($this->params["form"]);
                $validate ->addRule("order_person","string", array("min" => 3, "max" => 255, "id" => "order_person"))
                            ->addRule("phone","numeric", array("min" => 10000000, "max" => 9999999999999, "id" => "phone"))
                            ->addRule("address","string", array("min" => 3, "max" => 255, "id" => "address"));
                $validate->run();
                $this->view->result = $validate->showResult();
                if(!$validate->isValid())
                {
                    $this->view->errors = $validate->showError();
                    $this->cart_action();
                }
                else
                {
                    $this->db->save_item($this->params,array("task" => "save_cart"));
                }
            }
            else
            {
                $this->cart_action();
            }    
        }
        else
        {
            $this->cart_action();
        }
        
    }

    public function history_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $total_items = $this->db->count_items($this->params,array("task" => "history"));
        $config = array
        (
            "items_per_page"      => 3,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->view->items = $this->db->list_items($this->params,array("task" => "get_history_cart")); 
        $this->view->render("user/history");
    }

    public function purchased_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $total_items = $this->db->count_items($this->params,array("task" => "purchased"));
        $config = array
        (
            "items_per_page"      => 3,
            "page_range"          => 3
        );
        $this->set_pagination($config); 
        $this->view->pagination = new pagination($total_items,$this->params["pagination"]);
        $this->template_obj->set_all('default/main','index.php','template.ini');
        $this->view->items = $this->db->list_items($this->params,array("task" => "get_purchased_order")); 
        $this->view->render("user/purchased");
    }

    public function verify_order_action()
    {
        $this->db->verify_order($this->params);
    }

    public function change_quantity_action()
    {
        $result = $this->db->change_quantity($this->params);
    }

    public function change_pass_action()
    {
        $this->template_obj->set_all('default/main','index.php','template.ini');
        if(isset($this->params["form"]["submit"]))
        {
            $pass = $_SESSION["user"]["info"]["password"];
            if($this->params["form"]["recent_pass"] != null)
            {
                if($this->params["form"]["recent_pass"] == $pass)
                {
                    $validate = new validate($this->params["form"]);
                    $validate->addRule("new_password","password",array("id" => "new_password"))
                            ->addRule("rewrite_new_password","password",array("id" => "rewrite_new_password"));
                    $validate->run();
                    $this->view->result = $validate->showResult();
                    if(!$validate->isValid())
                    {
                        $this->view->errors = $validate->showError();
                    }
                    else
                    {   
                        if($this->params["form"]["new_password"] != $this->params["form"]["rewrite_new_password"])
                        {
                            Session::set("message_error_pass","New password not equal rewrite new pass word");
                            $this->view->render("user/change_pass");
                        }
                        else
                        {
                            $this->params["form"]["password"] = $this->params["form"]["new_password"];
                            $this->params["form"]["modified"] = date("Y-m-d H:m:s",time());
                            $this->params["form"]["modified_by"] = $_SESSION["user"]["info"]["id"];
                            $result_merge = array_intersect_key($this->params["form"],array_flip($this->_columns));
                            $this->db->save_item($result_merge,array("task" => "change_pass"));
                            helper::redirect("default","user","change_pass");
                        }
                    }
                }
                else
                {
                    Session::set("message_error_pass","Password incorrect, please rewrite!");
                    $this->view->render("user/change_pass");
                }
            }
            
            
        }
        $this->view->render("user/change_pass");
    }
}


   